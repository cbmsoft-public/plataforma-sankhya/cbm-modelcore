package com.cbmsoft.modelcore.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.cbmsoft.modelcore.CbmModelException;
import com.cbmsoft.modelcore.utils.CadastroUtils;
import com.sankhya.util.StringUtils;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.jape.wrapper.fluid.FluidCreateVO;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.modelcore.util.EnderecoUtils;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;

public class Endereco {
	private String						cep;
	private String						nomePais;
	private BigDecimal					codPais = BigDecimal.ZERO;
	private String						unidadeFederativa;
	private BigDecimal					codigoUF = BigDecimal.ZERO;
	private BigDecimal					codigoIbge = BigDecimal.ZERO;
	private String						nomeCidade;
	private String						nomeCidadeAbrev;
	private String						nomeBairro;
	private String						nomeBairroAbrev;
	private String						nomeEndereco;
	private String						nomeEnderecoAbrev;
	private String						tipoLogradouro;
	private String						numero;
	private String						complemento;
	private BigDecimal					codigoCidade = BigDecimal.ZERO;
	private BigDecimal					codigoBairro = BigDecimal.ZERO;
	private BigDecimal					codigoEndereco = BigDecimal.ZERO;
	private boolean						isInserirCidade = false;
	private boolean						isInserirBairro = false;
	private boolean						isInserirEndereco = false;
	private boolean						isInserirCep = false;
	
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNomePais() {
		return nomePais;
	}

	public void setNomePais(String nomePais) {
		this.nomePais = nomePais;
	}

	public BigDecimal getCodPais() {
		return codPais;
	}

	public void setCodPais(BigDecimal codPais) {
		this.codPais = codPais;
	}

	public String getUnidadeFederativa() {
		return unidadeFederativa;
	}

	public void setUnidadeFederativa(String unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}

	public BigDecimal getCodigoUF() {
		return codigoUF;
	}

	public void setCodigoUF(BigDecimal codigoUF) {
		this.codigoUF = codigoUF;
	}

	public BigDecimal getCodigoIbge() {
		return codigoIbge;
	}

	public void setCodigoIbge(BigDecimal codigoIbge) {
		this.codigoIbge = codigoIbge;
	}

	public String getNomeCidade() {
		return nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}

	public String getNomeCidadeAbrev() {
		return nomeCidadeAbrev;
	}

	public void setNomeCidadeAbrev(String nomeCidadeAbrev) {
		this.nomeCidadeAbrev = nomeCidadeAbrev;
	}

	public String getNomeBairro() {
		return nomeBairro;
	}

	public void setNomeBairro(String nomeBairro) {
		this.nomeBairro = nomeBairro;
	}

	public String getNomeBairroAbrev() {
		return nomeBairroAbrev;
	}

	public void setNomeBairroAbrev(String nomeBairroAbrev) {
		this.nomeBairroAbrev = nomeBairroAbrev;
	}

	public String getNomeEndereco() {
		return nomeEndereco;
	}

	public void setNomeEndereco(String nomeEndereco) {
		this.nomeEndereco = nomeEndereco;
	}

	public String getNomeEnderecoAbrev() {
		return nomeEnderecoAbrev;
	}

	public void setNomeEnderecoAbrev(String nomeEnderecoAbrev) {
		this.nomeEnderecoAbrev = nomeEnderecoAbrev;
	}

	public String getTipoLogradouro() {
		return tipoLogradouro;
	}

	public void setTipoLogradouro(String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public BigDecimal getCodigoCidade() {
		return codigoCidade;
	}

	public void setCodigoCidade(BigDecimal codigoCidade) {
		this.codigoCidade = codigoCidade;
	}

	public BigDecimal getCodigoBairro() {
		return codigoBairro;
	}

	public void setCodigoBairro(BigDecimal codigoBairro) {
		this.codigoBairro = codigoBairro;
	}

	public BigDecimal getCodigoEndereco() {
		return codigoEndereco;
	}

	public void setCodigoEndereco(BigDecimal codigoEndereco) {
		this.codigoEndereco = codigoEndereco;
	}

	public boolean isInserirCidade() {
		return isInserirCidade;
	}

	public void setInserirCidade(boolean isInserirCidade) {
		this.isInserirCidade = isInserirCidade;
	}

	public boolean isInserirBairro() {
		return isInserirBairro;
	}

	public void setInserirBairro(boolean isInserirBairro) {
		this.isInserirBairro = isInserirBairro;
	}

	public boolean isInserirEndereco() {
		return isInserirEndereco;
	}

	public void setInserirEndereco(boolean isInserirEndereco) {
		this.isInserirEndereco = isInserirEndereco;
	}

	public boolean isInserirCep() {
		return isInserirCep;
	}

	public void setInserirCep(boolean isInserirCep) {
		this.isInserirCep = isInserirCep;
	}

	public void normalize() throws Exception {
		if (nomePais != null) {
			this.nomePais = nomePais.trim();
			this.nomePais = StringUtils.removePontuacao(nomePais);
			this.nomePais = StringUtils.removerCaracteresEspeciais(nomePais);
			this.nomePais = StringUtils.replaceAccentuatedChars(nomePais);
			this.nomePais = nomePais.toUpperCase();
		}
		if (nomeCidade != null) {
			this.nomeCidade = nomeCidade.trim();
			this.nomeCidade = StringUtils.removePontuacao(nomeCidade);
			this.nomeCidade = StringUtils.removerCaracteresEspeciais(nomeCidade);
			this.nomeCidade = StringUtils.replaceAccentuatedChars(nomeCidade);
			this.nomeCidade = nomeCidade.toUpperCase();
		}
		if (nomeCidadeAbrev != null) {
			this.nomeCidadeAbrev = nomeCidadeAbrev.trim();
			this.nomeCidadeAbrev = StringUtils.removePontuacao(nomeCidadeAbrev);
			this.nomeCidadeAbrev = StringUtils.removerCaracteresEspeciais(nomeCidadeAbrev);
			this.nomeCidadeAbrev = StringUtils.replaceAccentuatedChars(nomeCidadeAbrev);
			this.nomeCidadeAbrev = nomeCidadeAbrev.toUpperCase();
		}
		if (nomeBairro != null) {
			this.nomeBairro = nomeBairro.trim();
			this.nomeBairro = StringUtils.removePontuacao(nomeBairro);
			this.nomeBairro = StringUtils.removerCaracteresEspeciais(nomeBairro);
			this.nomeBairro = StringUtils.replaceAccentuatedChars(nomeBairro);
			this.nomeBairro = nomeBairro.toUpperCase();
		}
		if (nomeBairroAbrev != null) {
			this.nomeBairroAbrev = nomeBairroAbrev.trim();
			this.nomeBairroAbrev = StringUtils.removePontuacao(nomeBairroAbrev);
			this.nomeBairroAbrev = StringUtils.removerCaracteresEspeciais(nomeBairroAbrev);
			this.nomeBairroAbrev = StringUtils.replaceAccentuatedChars(nomeBairroAbrev);
			this.nomeBairroAbrev = nomeBairroAbrev.toUpperCase();
		}
		if (nomeEndereco != null) {
			this.nomeEndereco = nomeEndereco.trim();
			this.nomeEndereco = StringUtils.removePontuacao(nomeEndereco);
			this.nomeEndereco = StringUtils.removerCaracteresEspeciais(nomeEndereco);
			this.nomeEndereco = StringUtils.replaceAccentuatedChars(nomeEndereco);
			this.nomeEndereco = nomeEndereco.toUpperCase();
		}
		if (nomeEnderecoAbrev != null) {
			this.nomeEnderecoAbrev = nomeEnderecoAbrev.trim();
			this.nomeEnderecoAbrev = StringUtils.removePontuacao(nomeEnderecoAbrev);
			this.nomeEnderecoAbrev = StringUtils.removerCaracteresEspeciais(nomeEnderecoAbrev);
			this.nomeEnderecoAbrev = StringUtils.replaceAccentuatedChars(nomeEnderecoAbrev);
			this.nomeEnderecoAbrev = nomeEnderecoAbrev.toUpperCase();
		}
		if (complemento != null) {
			this.complemento = complemento.trim();
			this.complemento = StringUtils.removePontuacao(complemento);
			this.complemento = StringUtils.removerCaracteresEspeciais(complemento);
			this.complemento = StringUtils.replaceAccentuatedChars(complemento);
			this.complemento = complemento.toUpperCase();
		}
		if (cep != null) {
			this.cep = cep.trim();
			this.cep = StringUtils.removePontuacao(cep);
			this.cep = StringUtils.removerCaracteresEspeciais(cep);
			this.cep = StringUtils.replaceAccentuatedChars(cep);
			this.cep = StringUtils.replaceString(cep, "-", "");
		}
		if (numero != null) {
			this.numero = numero.trim();
			this.numero = StringUtils.removePontuacao(numero);
			this.numero = StringUtils.removerCaracteresEspeciais(numero);
			this.numero = StringUtils.replaceAccentuatedChars(numero);
			this.numero = numero.toUpperCase();
		}
	}
	
	public void buildEndereco() throws Exception {
		EntityFacade dwfFacade = EntityFacadeFactory.getDWFFacade();
		
		this.codigoUF = CadastroUtils.findCodigoUF(unidadeFederativa);
		if (codigoUF == null)
			throw new CbmModelException(String.format("UF %s n�o encontrada", codigoUF));
		
		Collection<?> enderecoTk = new ArrayList(StringUtils.buildTokens(nomeEndereco, " ", new StringUtils.TokenBuilder() {
			public Object buildToken(String token) {
				return token;
			}
		}));
		
		for (Object token:enderecoTk) {
			if (EnderecoUtils.isLogradouroToken((String) token)) {
				this.tipoLogradouro = (String) token;
				this.nomeEndereco = StringUtils.replaceString(nomeEndereco, tipoLogradouro + " ", "");
			}
			break;
		}
		
		this.codigoCidade = EnderecoUtils.findCodigoCidadeByDescriptionAndUF(nomeCidade, codigoUF, dwfFacade, cep);
		if (codigoCidade == null || codigoCidade.longValue() == 0L)
			this.codigoCidade = insertCidade();
		
		this.codigoBairro = EnderecoUtils.findCodigoBairroByDescription(nomeBairro, dwfFacade, cep);
		if (codigoBairro == null || codigoBairro.longValue() == 0L)
			this.codigoBairro = insertBairro();
		
		this.codigoEndereco = EnderecoUtils.findCodigoEnderecoByDescription(nomeEndereco, dwfFacade, cep);
		if (codigoEndereco == null || codigoEndereco.longValue() == 0L)
			this.codigoEndereco = insertEndereco();
		
	}

	private BigDecimal insertCidade() throws Exception {
		JapeWrapper tabelaDAO = JapeFactory.dao(DynamicEntityNames.CIDADE);
		
		DynamicVO tabelaVO = tabelaDAO
				.create()
				.set("NOMECID", nomeCidade)
				.set("UF", codigoUF)
				.save();
		
//		String msg = String.format("Inclusao da cidade %s-%s", cidadeVO.asBigDecimal("CODCID"), cidadeVO.asString("NOMECID"));
		return tabelaVO.asBigDecimal("CODCID");
	}

	private BigDecimal insertBairro() throws Exception {
		JapeWrapper tabelaDAO = JapeFactory.dao(DynamicEntityNames.BAIRRO);
		
		DynamicVO tabelaVO = tabelaDAO
				.create()
				.set("NOMEBAI", nomeBairro)
				.save();
		
//		String msg = String.format("Inclusao da bairro %s-%s", tabelaVO.asBigDecimal("CODCID"), tabelaVO.asString("NOMECID"));
		return tabelaVO.asBigDecimal("CODBAI");
	}

	private BigDecimal insertEndereco() throws Exception {
		JapeWrapper tabelaDAO = JapeFactory.dao(DynamicEntityNames.ENDERECO);
		
		DynamicVO tabelaVO = tabelaDAO
				.create()
				.set("NOMEEND", nomeEndereco)
				.set("TIPO", tipoLogradouro)
				.save();
		
//		String msg = String.format("Inclusao da bairro %s-%s", tabelaVO.asBigDecimal("CODCID"), tabelaVO.asString("NOMECID"));
		return tabelaVO.asBigDecimal("CODEND");
	}

	@Override
	public String toString() {
		return String.format(
				"Endereco [cep=%s, nomePais=%s, codPais=%s, unidadeFederativa=%s, codigoUF=%s, codigoIbge=%s, nomeCidade=%s, nomeCidadeAbrev=%s, nomeBairro=%s, nomeBairroAbrev=%s, nomeEndereco=%s, nomeEnderecoAbrev=%s, tipoLogradouro=%s, numero=%s, complemento=%s, codigoCidade=%s, codigoBairro=%s, codigoEndereco=%s, isInserirCidade=%s, isInserirBairro=%s, isInserirEndereco=%s, isInserirCep=%s]",
				cep, nomePais, codPais, unidadeFederativa, codigoUF, codigoIbge, nomeCidade, nomeCidadeAbrev,
				nomeBairro, nomeBairroAbrev, nomeEndereco, nomeEnderecoAbrev, tipoLogradouro, numero, complemento,
				codigoCidade, codigoBairro, codigoEndereco, isInserirCidade, isInserirBairro, isInserirEndereco,
				isInserirCep);
	}
	

}

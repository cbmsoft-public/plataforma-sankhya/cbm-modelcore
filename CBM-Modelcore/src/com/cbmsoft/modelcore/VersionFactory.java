package com.cbmsoft.modelcore;

public class VersionFactory {
	private static final String MASK = "%1$d.%2$db%3$03d";
	
	private String						className = "cbmSoft";
	private int							major = 0;
	private int							minor = 0;
	private int							patch = 0;
	
	private String					completeVersion;

	public VersionFactory() {
		this.completeVersion = String.format(MASK, major, minor, patch);
	}
	
	public VersionFactory(int major, int minor, int patch) {
		super();
		this.major = major;
		this.minor = minor;
		this.patch = patch;
		this.completeVersion = String.format(MASK, major, minor, patch);
	}

	public VersionFactory(String className, int major, int minor, int patch) {
		super();
		this.className = className;
		this.major = major;
		this.minor = minor;
		this.patch = patch;
		this.completeVersion = String.format(MASK, major, minor, patch);
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getMajor() {
		return major;
	}

	public void setMajor(int major) {
		this.major = major;
	}

	public int getMinor() {
		return minor;
	}

	public void setMinor(int minor) {
		this.minor = minor;
	}

	public int getPatch() {
		return patch;
	}

	public void setPatch(int patch) {
		this.patch = patch;
	}

	public String getCompleteVersion() {
		return completeVersion;
	}

	public void setCompleteVersion(String completeVersion) {
		this.completeVersion = completeVersion;
	}

	@Override
	public String toString() {
		return String.format(
				"[%s %s]", className, completeVersion);
	}

}

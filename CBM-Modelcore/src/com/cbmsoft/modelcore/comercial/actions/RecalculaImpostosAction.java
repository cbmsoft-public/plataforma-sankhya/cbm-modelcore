package com.cbmsoft.modelcore.comercial.actions;

import java.math.BigDecimal;
import java.util.logging.Logger;

import com.cbmsoft.modelcore.LoggerFactory;
import com.cbmsoft.modelcore.comercial.helpers.CentralHelper;

import br.com.sankhya.extensions.actionbutton.AcaoRotinaJava;
import br.com.sankhya.extensions.actionbutton.ContextoAcao;
import br.com.sankhya.extensions.actionbutton.Registro;

public class RecalculaImpostosAction implements AcaoRotinaJava {
	final private Logger					logger = new LoggerFactory(this.getClass().getCanonicalName()).getLogger();

	private String							mensagemRetorno;
	private CentralHelper					central;
	private BigDecimal						nuNota;

	@Override
	public void doAction(ContextoAcao contexto) throws Exception {
		
		try {
			Registro[] linhas = contexto.getLinhas();
			
			for (Registro linha : linhas) {
				nuNota = (BigDecimal) linha.getCampo("NUNOTA");
				
				central = new CentralHelper();
				central.recalculaImposto(nuNota);
			}
			
			this.mensagemRetorno = "Lan�amentos processados.<br>Foi efetuado rec�lculo de impostos.";

		} catch (Exception e) {
			this.mensagemRetorno = "N�o foi poss�vel efetuar rec�lculo.<br>Verificar log.";
			
			if (e.getLocalizedMessage() != null)
				this.mensagemRetorno += ".<br><b>Exception</b>: " + e.getLocalizedMessage();

			logger.severe(mensagemRetorno);
			e.printStackTrace();
		} finally {
			contexto.setMensagemRetorno(mensagemRetorno);
		}
		
	}

}

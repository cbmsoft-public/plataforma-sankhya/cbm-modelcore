package com.cbmsoft.modelcore.comercial.helpers;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Logger;

import com.cbmsoft.modelcore.LoggerFactory;

import br.com.sankhya.jape.core.JapeSession;
import br.com.sankhya.jape.core.JapeSession.SessionHandle;
import br.com.sankhya.jape.util.JapeSessionContext;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.vo.EntityVO;
import br.com.sankhya.jape.vo.PrePersistEntityState;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.modelcore.auth.AuthenticationInfo;
import br.com.sankhya.modelcore.comercial.BarramentoRegra;
import br.com.sankhya.modelcore.comercial.CentralFaturamento;
import br.com.sankhya.modelcore.comercial.CentralFinanceiro;
import br.com.sankhya.modelcore.comercial.CentralItemNota;
import br.com.sankhya.modelcore.comercial.ConfirmacaoNotaHelper;
import br.com.sankhya.modelcore.comercial.impostos.ImpostosHelpper;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.sankhya.modelcore.util.ListenerParameters;

public class CentralHelper {
	final private Logger					logger = new LoggerFactory(this.getClass().getCanonicalName()).getLogger();

	public static PrePersistEntityState confirmaNotaSilencioso(BigDecimal nuNota) throws Exception {
		final BarramentoRegra barramento = BarramentoRegra.build(CentralFaturamento.class, "regrasConfirmacaoSilenciosa.xml", AuthenticationInfo.getCurrent());
		barramento.setValidarSilencioso(true);
		
		PrePersistEntityState pre = ConfirmacaoNotaHelper.confirmarNota(nuNota, barramento, true);
		
		return pre;
	}

	public void recalculaImposto(final BigDecimal nuNota) {
		JapeSessionContext.putProperty(ListenerParameters.CENTRAIS, Boolean.TRUE);
		
		SessionHandle hnd = null;
		
		try {
			hnd = JapeSession.getCurrentSession().getTopMostHandle();
			
			hnd.execWithTX(new JapeSession.TXBlock() {
				
				@Override
				public void doWithTx() throws Exception {
					JapeWrapper itemNotaDAO = JapeFactory.dao(DynamicEntityNames.ITEM_NOTA);
					
					if (nuNota != null) {
						CentralItemNota centralItem = new CentralItemNota();

						Collection<DynamicVO> itensVO = itemNotaDAO.find("this.NUNOTA = ?", new Object[]{ nuNota });
						for (DynamicVO itemVO : itensVO) {
							centralItem.recalcularValores("QTDNEG", itemVO.asBigDecimalOrZero("QTDNEG").toString(), itemVO, nuNota);
							EntityFacadeFactory.getDWFFacade().createEntity(DynamicEntityNames.ITEM_NOTA, (EntityVO) itemVO);
						}
						
						ImpostosHelpper impHelpper = new ImpostosHelpper();
						impHelpper.totalizarNota(nuNota);
						impHelpper.salvarNota();
						CentralFinanceiro centralFinanceiro = new CentralFinanceiro();
						centralFinanceiro.inicializaNota(nuNota);
						centralFinanceiro.refazerFinanceiro();
					}
					
				}
			});
			
		} catch (Exception e) {
			String msg = String.format("N�o foi poss�vel efetuar rec�lculo do lan�amento NUNOTA %s.", nuNota);
			
			if (e.getLocalizedMessage() != null)
				msg += ".<br><b>Exception</b>: " + e.getLocalizedMessage();

			logger.severe(msg);
//			e.printStackTrace();
		} finally {
			hnd = null;
			
			JapeSessionContext.putProperty(ListenerParameters.CENTRAIS, Boolean.FALSE);
		}
		
	}

}

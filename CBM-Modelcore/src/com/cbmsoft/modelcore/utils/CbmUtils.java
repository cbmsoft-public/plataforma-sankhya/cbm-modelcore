package com.cbmsoft.modelcore.utils;

import com.cbmsoft.modelcore.VersionFactory;

public class CbmUtils {
	
	private CbmUtils() {
		
	}

	public static String getVersion(String className) {
		if (className == null)
			className = "CBM-Modelcore";
		
		VersionFactory version = new VersionFactory(className, 1, 2, 4);
		
		return version.toString();
	}
	
	public static String getVersion(String className, int major, int minor, int patch) {
		if (className == null)
			className = "CBM-Modelcore";
		
		VersionFactory version = new VersionFactory(className, major, minor, patch);
		
		return version.toString();		
	}

}

package com.cbmsoft.modelcore.utils;

import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.jape.metadata.EntityMetaData;
import br.com.sankhya.jape.sql.NativeSql;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;

public class DatabaseUtils {
	public static final long			TIME_TO_SLEEP	= Long.parseLong(System.getProperty("wms.integracao.sleep.ret.pedidos", "1000"));

	public static boolean isOracle() {
		boolean							isOracle = false;
		
		try {
			isOracle = EntityFacadeFactory.getDWFFacade().getJdbcWrapper().getDialect() == EntityMetaData.ORACLE_DIALECT;
		} catch (Exception e) {
			
		}
		
		return isOracle;	
	}

	public static void setDeadlockPriotityLow (JdbcWrapper jdbc) throws Exception {
		NativeSql						lowDeadLock = null;
		
		try {
			if (!isOracle()) {
				lowDeadLock = new NativeSql(jdbc);
				lowDeadLock.executeUpdate("SET DEADLOCK_PRIORITY LOW");			
			}			
		} finally {
			NativeSql.releaseResources(lowDeadLock);
		}
	}
	
}

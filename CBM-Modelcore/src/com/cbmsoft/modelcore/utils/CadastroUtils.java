package com.cbmsoft.modelcore.utils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Logger;

import com.cbmsoft.modelcore.CbmModelException;
import com.cbmsoft.modelcore.LoggerFactory;
import com.cbmsoft.modelcore.MessageFactory;

import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.modelcore.util.DynamicEntityNames;

public class CadastroUtils {
	private static MessageFactory		msgI18N = new MessageFactory();
	private static Logger				logger = new LoggerFactory(CadastroUtils.class.getCanonicalName()).getLogger();

	public static BigDecimal findCodParcPorCnpj (String cnpj) throws CbmModelException {
		JapeWrapper						parceiroDAO = JapeFactory.dao(DynamicEntityNames.PARCEIRO);
		BigDecimal						codParc = BigDecimal.ZERO;
		
		String errMsg = String.format("%s CNPJ %s", msgI18N.getMessage("msgRecordNotFound"), cnpj);
		try {
			Collection<DynamicVO> parceirosVO = parceiroDAO.find("this.CNPJ = ?", cnpj);
			
			if (parceirosVO.size() > 1) {
				errMsg = String.format("%s CNPJ %s", msgI18N.getMessage("msgTooManyRecordsFound"), cnpj);
				
				throw new CbmModelException(errMsg);
				
			} else if (parceirosVO.size() == 0) {
				logger.info(errMsg);
			} else {
				codParc = parceirosVO.iterator().next().asBigDecimal("CODPARC");
			}
			
		} catch (Exception e) {
						
			if (e.getLocalizedMessage() != null)
				errMsg += ".<br><b>Exception</b>: " + e.getLocalizedMessage();

			logger.info(errMsg);
		}
		
		return codParc;
	}
	
	public static BigDecimal findCodigoUF (String uf) {
		BigDecimal codUF = null;
		
		JapeWrapper ufDAO = JapeFactory.dao(DynamicEntityNames.UNIDADE_FEDERATIVA);
		
		try {
			Collection<DynamicVO> ufsVO = ufDAO.find("ignorecase(this.UF) = ignorecase(?)", uf);
			
			if (ufsVO.size() == 1) {
				codUF = ufsVO.iterator().next().asBigDecimal("CODUF");
			} 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return codUF;
	}
}

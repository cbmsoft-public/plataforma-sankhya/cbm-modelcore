/*
 * Created on 22/02/2020
 *
 */
package com.cbmsoft.modelcore;

import java.io.Serializable;

import com.sankhya.util.StringUtils;

import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.sankhya.modelcore.util.I18nManager;

/**
 * @author CASSIO Menezes (cassio.menezes@outlook.com.br)
 *
 */
public class CbmModelException extends Exception implements Serializable {
	private static final long serialVersionUID = 1L;

	public CbmModelException(String message) {
		super(message);
	}

	public CbmModelException(Throwable cause) {
		super(cause);
	}

	public CbmModelException(String message, Throwable cause) {
		super(message, cause);
	}

	public static void throwMe(Throwable error) throws CbmModelException {
		if (error instanceof CbmModelException) {
			throw (CbmModelException) error;
		}
		
		String msg = error.getMessage();
		
		if (StringUtils.isEmpty(msg)) {
			JdbcWrapper jdbc = null;
			
			try {
				jdbc = EntityFacadeFactory.getDWFFacade().getJdbcWrapper();
				jdbc.openSession();
				
				I18nManager i18n = I18nManager.getInstance();
				
				if (error instanceof NullPointerException) {
					msg = i18n.getDescricao(jdbc, "msgErroInterno", "Erro interno (NPE)");
				} else {
					msg = i18n.getDescricao(jdbc, "msgSemMessagErro", "<sem mensagem de erro>");
				}
			} catch (Exception e) {
				msg = "<sem mensagem de erro>";
			}finally{
				JdbcWrapper.closeSession(jdbc);
			}
		}

		CbmModelException exception = new CbmModelException(msg);
		
		exception.initCause(error);
		
		throw exception;
	}
	
	public static String buildErrorMessage(Throwable error, String message) {
		StringBuilder sbMessage = new StringBuilder();
		sbMessage.append(message);
		
		if (error.getLocalizedMessage() != null)
			sbMessage.append(".<br><b>Exception</b>: " + error.getLocalizedMessage());

		return sbMessage.toString();
	}
}

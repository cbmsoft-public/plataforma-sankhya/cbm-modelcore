package com.cbmsoft.modelcore;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageFactory {
	private Locale				locPt = new Locale("pt", "BR");
	private String				baseName;
	private ResourceBundle		rbMessages;
	
	public MessageFactory() {
		this(null);
	}
	
	public MessageFactory(String name) {
		if (name == null)
			name = String.format("%s.i18n",this.getClass().getPackage().getName());
		
		this.baseName = String.format("%s/MessagesBundle", name);
		this.rbMessages = ResourceBundle.getBundle(baseName, locPt);
	}

	public Locale getLocPt() {
		return locPt;
	}

	public void setLocPt(Locale locPt) {
		this.locPt = locPt;
	}

	public String getMessage(String key) {
		return rbMessages.getString(key);
	}
}

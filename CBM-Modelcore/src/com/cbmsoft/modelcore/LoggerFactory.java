package com.cbmsoft.modelcore;

import java.util.logging.Logger;

public class LoggerFactory {
	private final Logger					logger;

	public LoggerFactory() {
		this.logger = Logger.getLogger(this.getClass().getCanonicalName());
	}
	
	public LoggerFactory(String name) {
		this.logger = Logger.getLogger(name);
	}

	public final Logger getLogger() {
		return logger;
	}

	@Override
	public String toString() {
		return String.format("LoggerFactory [logger=%s]", logger.getName());
	}
	
}
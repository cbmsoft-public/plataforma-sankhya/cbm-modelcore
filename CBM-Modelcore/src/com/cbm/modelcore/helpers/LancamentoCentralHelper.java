package com.cbm.modelcore.helpers;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.bmp.PersistentLocalEntity;
import br.com.sankhya.jape.util.JapeSessionContext;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.vo.EntityVO;
import br.com.sankhya.jape.vo.PrePersistEntityState;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.modelcore.MGEModelException;
import br.com.sankhya.modelcore.auth.AuthenticationInfo;
import br.com.sankhya.modelcore.comercial.BarramentoRegra;
import br.com.sankhya.modelcore.comercial.CentralFaturamento;
import br.com.sankhya.modelcore.comercial.ComercialUtils;
import br.com.sankhya.modelcore.comercial.ConfirmacaoNotaHelper;
import br.com.sankhya.modelcore.comercial.LiberacaoSolicitada;
import br.com.sankhya.modelcore.comercial.centrais.CACHelper;
import br.com.sankhya.modelcore.comercial.impostos.ImpostosHelpper;
import br.com.sankhya.modelcore.dwfdata.vo.ItemNotaVO;
import br.com.sankhya.modelcore.dwfdata.vo.tsi.UsuarioVO;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.sankhya.ws.ServiceContext;

import com.cbm.modelcore.utils.LogUtils;
import com.sankhya.util.BigDecimalUtil;
import com.sankhya.util.TimeUtils;

/**
 * Funcionalidades para lan�amentos em centrais
 * 
 * @author Cassio MENEZES [cassio.menezes@dbconsultoria.com]
 * @version 4.5.0.05
 */
public class LancamentoCentralHelper {
	private LogUtils logAcao = new LogUtils(getClass().getSimpleName(), 4, 5, 0, 5);

	private static final String ENTITY_NAME_CAB = "CabecalhoNota";
	private static final String ENTITY_NAME_ITE = "ItemNota";
	private static final String ENTITY_NAME_PRO = "Produto";
	private static MathContext mathCtx = new MathContext(64, RoundingMode.HALF_UP);

	private EntityFacade			dwfEntityFacade;
	private CACHelper				cacHelper;
	private BarramentoRegra			bRegras;
	private ImpostosHelpper			impostosHelpper;
	private Timestamp				dataAtual;
	private ServiceContext			ctx;
	private BigDecimal				codEmp;
	private Timestamp				dtNeg;
	private String					serieNota;
	private BigDecimal				codTipOper;
	private Timestamp				dhTipoOperacao;
	private String					tipMov;
	private BigDecimal				codParc;
	private BigDecimal				codTipVenda;
	private Timestamp				dhTipVenda;
	private BigDecimal				codNat;
	private BigDecimal				codCenCus;
	private Map<String, Object>		camposCabecalhoMap;
	private DynamicVO				newCabVO;
	private DynamicVO				oldCabVO;
	private BigDecimal				nuNota;
	private BigDecimal				numeroNota;
	private boolean					isLancamentoCte;
	private BigDecimal				atualizacaoEstoque;
	private String					reservaEstoque;

	private BigDecimal				codUsu;
	private AuthenticationInfo		oldAuthInfo;
	private AuthenticationInfo		authInfo;
	private UsuarioVO				oldUsuVO;
	private UsuarioVO				newUsuVO;
	
	public LancamentoCentralHelper(ServiceContext ctx,
			BigDecimal nuNota) throws Exception {

	    this.dwfEntityFacade = EntityFacadeFactory.getDWFFacade();

	    this.dataAtual = new Timestamp(System.currentTimeMillis());

	    this.numeroNota = nuNota;
	    this.ctx = ctx;
	    this.codUsu = BigDecimal.ZERO;

	    iniciarCacHelper();
	    
	    this.bRegras = null;
	}

	public LancamentoCentralHelper(ServiceContext ctx, 
			BigDecimal codEmp, Timestamp dtNeg, BigDecimal codTipOper,
			BigDecimal codParc, BigDecimal codTipVenda, BigDecimal codNat,
			BigDecimal codCenCus, String serieNota, 
			Map<String, Object> camposCabecalhoMap, BigDecimal codUsu) throws Exception {
		
		//identificador para log
	    //this.logAcao = new LogUtils(getClass().getSimpleName(), 4, 4, 0, 1);
		
	    this.dwfEntityFacade = EntityFacadeFactory.getDWFFacade();

	    this.dataAtual = new Timestamp(System.currentTimeMillis());

	    DynamicVO topVO = ComercialUtils.getTipoOperacao(codTipOper, dataAtual);
	    DynamicVO tpvVO = ComercialUtils.getTipoNegociacao(codTipVenda, dataAtual);

	    this.impostosHelpper = new ImpostosHelpper();
	    this.impostosHelpper.setForcarRecalculo(true);
	 
	    this.isLancamentoCte = isLancamentoCte(topVO);

	    this.numeroNota = BigDecimal.ZERO;

	    this.ctx = ctx;
	    this.codEmp = codEmp;
	    this.dtNeg = dtNeg;
	    this.serieNota = serieNota;
	    this.codTipOper = codTipOper;
	    this.dhTipoOperacao = topVO.asTimestamp("DHALTER");
	    this.tipMov = topVO.asString("TIPMOV");
	    this.codParc = codParc;
	    this.codTipVenda = codTipVenda;
	    this.dhTipVenda = tpvVO.asTimestamp("DHALTER");
	    this.codNat = codNat;
	    this.codCenCus = codCenCus;
	    this.camposCabecalhoMap = camposCabecalhoMap;
	    this.codUsu = (codUsu == null) ? BigDecimal.ZERO : codUsu;

	    iniciarCacHelper();
		
	    this.bRegras = null;
	    this.newCabVO = buildCabecalhoVO();
	    this.oldCabVO = this.newCabVO;

		if (isLancamentoCte) {
		    incluirPreLancamento();
		} else {
		    incluirAlterarLancamento();
		}
		
		setAtualizacaoEstoque(topVO.asString("ATUALEST").trim());
	}

	public CACHelper getCacHelper() {
		return cacHelper;
	}

	public void setCacHelper(CACHelper cacHelper) {
		this.cacHelper = cacHelper;
	}

	public ServiceContext getCtx() {
		return ctx;
	}

	public void setCtx(ServiceContext ctx) {
		this.ctx = ctx;
	}
	
	public void setServiceContext() {
		ServiceContext					svcContext = null;

		try {
			svcContext = ServiceContext.getCurrent();
		} catch (NullPointerException npe) {            
           	npe.printStackTrace();
		} catch (Exception e) {
			String errorMessage = e.getMessage();
			
            logAcao.registraLog(errorMessage);

			e.printStackTrace();				
		}
		
		if (svcContext == null) {
            svcContext = new ServiceContext(null);
            setAuthentication(BigDecimal.ZERO);
		}

		this.ctx = svcContext;
		this.ctx.makeCurrent();
	}
	
	/**
	 * Define autenticacao para execucao
	 * 
	 * @param codUsu <b>BigDecimal</b> informar um codigo valido de usuario
	 */
	public void setAuthentication(BigDecimal codUsu) {
		/*
		 * armazenar informacoes do usuario corrente para restauracao posterior
		 */		
		try {
			oldAuthInfo = (AuthenticationInfo) ServiceContext.getCurrent().getAutentication();			
		} catch (NullPointerException npe) {            
			npe.printStackTrace();            	
		}

		try {
			newUsuVO = ( UsuarioVO ) (( DynamicVO ) EntityFacadeFactory.getDWFFacade().findEntityByPrimaryKeyAsVO(DynamicEntityNames.USUARIO, new Object [] { codUsu })).wrapInterface(UsuarioVO.class);
		} catch (Exception e) {
			String errorMessage = e.getMessage();

			logAcao.registraLog("newUsuVO: Usuario nao encontrado: " + codUsu.toString());
            logAcao.registraLog(errorMessage);

			e.printStackTrace();				
		}
		
		/*
		 * Se nao encontar usuario forcar usuario 0-SUP
		 */
		if (newUsuVO == null) {
			try {
				newUsuVO = ( UsuarioVO ) (( DynamicVO ) EntityFacadeFactory.getDWFFacade().findEntityByPrimaryKeyAsVO(DynamicEntityNames.USUARIO, new Object [] { BigDecimal.ZERO })).wrapInterface(UsuarioVO.class);
			} catch (Exception e) {
				String errorMessage = (e.getMessage() == null) ? "SEM MENSAGEM DE ERRO" : e.getMessage();
				
				logAcao.registraLog("newUsuVO: Usuario nao encontrado: " + codUsu.toString());
	            logAcao.registraLog(errorMessage);

				e.printStackTrace();				
				
				return;
			}			
		}

		/*
		 * Se nao houver usuario autenticado, forcar 0-SUP
		 */
		if (oldAuthInfo == null) {
			try {
				oldUsuVO = ( UsuarioVO ) (( DynamicVO ) EntityFacadeFactory.getDWFFacade().findEntityByPrimaryKeyAsVO(DynamicEntityNames.USUARIO, new Object [] { BigDecimal.ZERO })).wrapInterface(UsuarioVO.class);
			} catch (Exception e) {

				logAcao.registraLog("N�o foi poss�vel encontrar usuario 0.");

				e.printStackTrace();				
			
				return;
			}
		} else {
			oldUsuVO = oldAuthInfo.getUsuVO();			
		}

		this.authInfo =  new AuthenticationInfo(newUsuVO.getNOMEUSU(), newUsuVO.getCODUSU(), newUsuVO.getCODGRUPO(), new Integer(Integer.MAX_VALUE));
		this.authInfo.makeCurrent();
		try {
			this.ctx.setAutentication(this.authInfo);
		} catch (Exception e) {
			String errorMessage = (e.getMessage() == null) ? "SEM MENSAGEM DE ERRO" : e.getMessage();

			logAcao.registraLog("Exception ctx.setAutentication");
			logAcao.registraLog(errorMessage);

			e.printStackTrace();				
		}

		JapeSessionContext.putProperty("usuario_logado", this.authInfo.getUserID());
        JapeSessionContext.putProperty("emp_usu_logado", BigDecimalUtil.getValueOrZero(newUsuVO.getCODEMP()));
        JapeSessionContext.putProperty("dh_atual", new Timestamp(System.currentTimeMillis()));
        JapeSessionContext.putProperty("d_atual", new Timestamp(TimeUtils.getToday()));
        JapeSessionContext.putProperty("usuarioVO", newUsuVO);
        JapeSessionContext.putProperty("authInfo", this.authInfo);       
	}

	/**
	 * Restaura autenticacao 
	 */
	public void releaseAuthentication() {
		ctx.unregistry();
		AuthenticationInfo.unregistry();
	
		if (oldUsuVO != null) {
			logAcao.registraLog("Restaurando usuario " + oldUsuVO.getCODUSU().toString());

			this.authInfo = new AuthenticationInfo(oldUsuVO.getNOMEUSU(), oldUsuVO.getCODUSU(), oldUsuVO.getCODGRUPO(), new Integer(Integer.MAX_VALUE));
			this.authInfo.makeCurrent();

			JapeSessionContext.putProperty("usuario_logado", authInfo.getUserID());
	        JapeSessionContext.putProperty("emp_usu_logado", BigDecimalUtil.getValueOrZero(oldUsuVO.getCODEMP()));
	        JapeSessionContext.putProperty("dh_atual", new Timestamp(System.currentTimeMillis()));
	        JapeSessionContext.putProperty("d_atual", new Timestamp(TimeUtils.getToday()));
	        JapeSessionContext.putProperty("usuarioVO", oldUsuVO);
	        JapeSessionContext.putProperty("authInfo", authInfo);
	        
        	logAcao.registraLog("Usuario " + oldUsuVO.getCODUSU().toString() + "-" + oldUsuVO.getNOMEUSU() + " autenticado.");
		}
		
		this.oldUsuVO = null;
		this.newUsuVO = null;
		this.oldAuthInfo = null;
		this.authInfo = null;
		this.ctx = null;
	}

	public DynamicVO getNewCabVO() {
		return newCabVO;
	}

	public BigDecimal getNuNota() {
		return nuNota;
	}

	public void setNumeroNota(BigDecimal numeroNota) {
		this.numeroNota = numeroNota;
	}

	public boolean isLancamentoCte(DynamicVO topVO) {
		boolean isLancCte = false;
		
		BigDecimal codModDocCt = new BigDecimal(8);
		BigDecimal codModDocCte = new BigDecimal(57);
		BigDecimal codMOdDoc = topVO.asBigDecimal("CODMODDOC");
		
		isLancCte = (codModDocCt.equals(codMOdDoc) || codModDocCte.equals(codMOdDoc));
		
		return isLancCte;
	}
	
	private void setAtualizacaoEstoque(String atualizacaoEstoqueTop) {
		
		if ("N".equalsIgnoreCase(atualizacaoEstoqueTop)) {
			this.atualizacaoEstoque = BigDecimal.ZERO;
			this.reservaEstoque = "N";
		} else if ("E".equalsIgnoreCase(atualizacaoEstoqueTop)) {
			this.atualizacaoEstoque = BigDecimal.ONE;
			this.reservaEstoque = "N";
		} else if ("B".equalsIgnoreCase(atualizacaoEstoqueTop)) {
			this.atualizacaoEstoque = new BigDecimal(-1);
			this.reservaEstoque = "N";
		} else if ("R".equalsIgnoreCase(atualizacaoEstoqueTop)) {
			this.atualizacaoEstoque = BigDecimal.ONE;
			this.reservaEstoque = "S";
		}
		
	}

	/**
	 * Instanciar classe CACHelper. Necessario autenticacao e context
	 * 
	 * @throws Exception
	 */
	private void iniciarCacHelper() throws Exception {
		this.cacHelper = new CACHelper();
		
		try {
			CACHelper.setupContext(ctx);			
		} catch (NullPointerException npe) {
			setServiceContext();
			setAuthentication(codUsu);
			
			CACHelper.setupContext(ctx);
		}
	}

	private DynamicVO buildCabecalhoVO () throws Exception {
		DynamicVO newCabVO = (DynamicVO) dwfEntityFacade.getDefaultValueObjectInstance(ENTITY_NAME_CAB);

		newCabVO.setPrimaryKey(null);
		newCabVO.setProperty("NUNOTA", null);

		newCabVO.setProperty("DANFE", null);
		newCabVO.setProperty("CHAVENFE", null);
		newCabVO.setProperty("NUMALEATORIO", null);
		newCabVO.setProperty("NUMPROTOC", null);
		newCabVO.setProperty("DHPROTOC", null);
		newCabVO.setProperty("NULOTENFE", null);
		newCabVO.setProperty("STATUSNFE", null);
		newCabVO.setProperty("TPEMISNFE", null);
		newCabVO.setProperty("NUMREGDPEC", null);
		newCabVO.setProperty("DHREGDPEC", null);
		newCabVO.setProperty("CODMAQ", null);
		newCabVO.setProperty("NUNOTAPEDFRET", null);
		newCabVO.setProperty("DTPREVENT", null);
		newCabVO.setProperty("NUMNFSE", null);
		newCabVO.setProperty("TPEMISNFSE", null);
		newCabVO.setProperty("NULOTENFSE", null);
		newCabVO.setProperty("STATUSNFSE", null);
		newCabVO.setProperty("DTFATUR", null);
		newCabVO.setProperty("DTENTSAI", null);
		newCabVO.setProperty("DTCONTAB", null);
		newCabVO.setProperty("NUMNOTA", numeroNota);
		newCabVO.setProperty("STATUSNOTA", "A");
		newCabVO.setProperty("VLRNOTA", BigDecimal.ZERO);
		newCabVO.setProperty("TIPMOV", tipMov);
		newCabVO.setProperty("CODTIPOPER", codTipOper);
		newCabVO.setProperty("DHTIPOPER", dhTipoOperacao);
		newCabVO.setProperty("CODTIPVENDA", codTipVenda);
		newCabVO.setProperty("DHTIPVENDA", dhTipVenda);
		newCabVO.setProperty("CODEMP", codEmp);
		newCabVO.setProperty("DTNEG", dtNeg);
		newCabVO.setProperty("DTMOV", dataAtual);
		newCabVO.setProperty("HRMOV", TimeUtils.timestamp2BigDecimal(dataAtual));
		newCabVO.setProperty("CODEMPNEGOC", codEmp);
		newCabVO.setProperty("CODPARC", codParc);
		newCabVO.setProperty("CODPARCTRANSP", null);
		newCabVO.setProperty("CODVEND", null);
		newCabVO.setProperty("CODNAT", codNat);
		newCabVO.setProperty("CODCENCUS", codCenCus);
		newCabVO.setProperty("SERIENOTA", serieNota);
		
		if (camposCabecalhoMap != null) {
			Set<String> nomesDeCampos = camposCabecalhoMap.keySet();
			
			for (String nomeCampo:nomesDeCampos) {
				newCabVO.setProperty(nomeCampo, camposCabecalhoMap.get(nomeCampo));
			}
		}
		
		return newCabVO;
	}
	
	private void incluirPreLancamento() throws Exception {
		PersistentLocalEntity newCabEntity =  dwfEntityFacade.createEntity(ENTITY_NAME_CAB, (EntityVO) newCabVO);

		this.newCabVO = (DynamicVO) newCabEntity.getValueObject();
		this.nuNota = newCabVO.asBigDecimal("NUNOTA");
	}
	
	public void incluirAlterarCabecalho() throws Exception {
		PersistentLocalEntity cabEntity = dwfEntityFacade.findEntityByPrimaryKey(ENTITY_NAME_CAB, new Object[] { nuNota });
		
		PrePersistEntityState cabState = PrePersistEntityState.build(dwfEntityFacade, ENTITY_NAME_CAB, newCabVO, oldCabVO, cabEntity);
		
		if (cabState == null) {
			throw new MGEModelException("N�o foi poss�vel inicializar o lan�amento!");
		} else {
			this.bRegras = cacHelper.incluirAlterarCabecalho(ctx, cabState);			
		}

		this.newCabVO = (DynamicVO) cabEntity.getValueObject();
		this.nuNota = newCabVO.asBigDecimal("NUNOTA");
	}

	private void incluirAlterarLancamento() throws Exception {
		
		PersistentLocalEntity newCabEntity =  dwfEntityFacade.createEntity(ENTITY_NAME_CAB, (EntityVO) newCabVO);
		
		PrePersistEntityState cabState = PrePersistEntityState.build(dwfEntityFacade, ENTITY_NAME_CAB, newCabVO, oldCabVO, newCabEntity);
		
		if (cabState == null) {
			throw new MGEModelException("N�o foi poss�vel inicializar o lan�amento!");
		} else {
			this.bRegras = cacHelper.incluirAlterarCabecalho(ctx, cabState);			
		}

		this.newCabVO = (DynamicVO) newCabEntity.getValueObject();
		this.nuNota = newCabVO.asBigDecimal("NUNOTA");
	}

	public class ItemNota {
		private BigDecimal				codProduto;
		private String					codVolume;
		private BigDecimal				quantidade;
		private BigDecimal				valorUnitario;
		private String					controle;
		private BigDecimal				codLocalOrigem;
		private BigDecimal				sequencia;
		
		private Map<String, Object>		camposItemMap;
		
		private DynamicVO				newItemVO;

		public ItemNota(BigDecimal codProduto, BigDecimal quantidade,
				BigDecimal valorUnitario, Map<String, Object> camposItemMap) throws Exception {
			
			init(codProduto, quantidade, valorUnitario, null, camposItemMap);
			
		}

		public ItemNota(BigDecimal codProduto, BigDecimal quantidade,
				BigDecimal valorUnitario, BigDecimal codLocalOrigem, Map<String, Object> camposItemMap) throws Exception {

			init(codProduto, quantidade, valorUnitario, codLocalOrigem, camposItemMap);

		}
		
		private void init(BigDecimal codProduto, BigDecimal quantidade,
				BigDecimal valorUnitario, BigDecimal codLocalOrigem, Map<String, Object> camposItemMap) throws Exception {

			this.codProduto = codProduto;
			// utilizar unidade padrao do produto
//			PersistentLocalEntity produtoEntity = dwfEntityFacade.findEntityByPrimaryKey(ENTITY_NAME_PRO, new Object[] { codProduto });
//			
//			DynamicVO produtoVO = (DynamicVO) produtoEntity.getValueObject();
			JapeWrapper produtoDAO = JapeFactory.dao(ENTITY_NAME_PRO);
			DynamicVO produtoVO = produtoDAO.findByPK(new Object[] { codProduto });
			
			this.codVolume = produtoVO.asString("CODVOL");
			int decQtd = produtoVO.asInt("DECQTD");
			int decVlr = produtoVO.asInt("DECVLR");

			this.quantidade = BigDecimalUtil.getRounded(quantidade, decQtd);
			this.valorUnitario = BigDecimalUtil.getRounded(valorUnitario, decVlr);
			if (camposItemMap == null)
				camposItemMap = new HashMap<String, Object>();
			
			this.camposItemMap = camposItemMap;
			
			this.controle = " ";
			this.codLocalOrigem = (codLocalOrigem == null) ? BigDecimal.ZERO : codLocalOrigem;
			
			this.newItemVO = buildItemVO();
			
			//logAcao.registraLog(String.format("before.codLocalOrigem %s", this.codLocalOrigem));

			PersistentLocalEntity newItemEntity = dwfEntityFacade.createEntity(ENTITY_NAME_ITE, (EntityVO) newItemVO);
			this.newItemVO = (DynamicVO) newItemEntity.getValueObject();

	        List<DynamicVO> itensFatura = new ArrayList<DynamicVO>();
			itensFatura.add(newItemVO);
	        cacHelper.incluirAlterarItem(nuNota, ctx, null, true, itensFatura);

	        this.sequencia = newItemVO.asBigDecimal("SEQUENCIA");
	        
			impostosHelpper.calcularImpostosItem((ItemNotaVO) newItemVO.wrapInterface(ItemNotaVO.class), codProduto);
			impostosHelpper.calcularTotalItens(newItemVO.asBigDecimal("NUNOTA"), false);

		}
		
		private DynamicVO buildItemVO () throws Exception {
			DynamicVO newItemVO = (DynamicVO) dwfEntityFacade.getDefaultValueObjectInstance(ENTITY_NAME_ITE);

			newItemVO.setProperty("NUNOTA", nuNota);

			if (camposCabecalhoMap == null || (camposCabecalhoMap != null && !camposItemMap.containsKey("USOPROD"))) {
				newItemVO.setProperty("USOPROD", "V");
			}
			newItemVO.setProperty("CODCFO", BigDecimal.ZERO);
			newItemVO.setProperty("QTDENTREGUE", BigDecimal.ZERO);
			newItemVO.setProperty("QTDCONFERIDA", BigDecimal.ZERO);
			newItemVO.setProperty("VLRCUS", BigDecimal.ZERO);
			newItemVO.setProperty("BASEIPI", BigDecimal.ZERO);
			newItemVO.setProperty("VLRIPI", BigDecimal.ZERO);
			newItemVO.setProperty("BASEICMS", BigDecimal.ZERO);
			newItemVO.setProperty("VLRICMS", BigDecimal.ZERO);
			newItemVO.setProperty("VLRDESC", BigDecimal.ZERO);
			newItemVO.setProperty("BASESUBSTIT", BigDecimal.ZERO);
			newItemVO.setProperty("VLRSUBST", BigDecimal.ZERO);
//			newItemVO.setProperty("CODTRIB", BigDecimal.ZERO);
			newItemVO.setProperty("CODOBSPADRAO", BigDecimal.ZERO);
			newItemVO.setProperty("ALIQICMS", BigDecimal.ZERO);
			newItemVO.setProperty("ALIQIPI", BigDecimal.ZERO);
			newItemVO.setProperty("CODUSU", AuthenticationInfo.getCurrent().getUserID());
			newItemVO.setProperty("STATUSNOTA", "A");
			newItemVO.setProperty("CODEMP", codEmp);
			newItemVO.setProperty("DTALTER", dataAtual);

			newItemVO.setProperty("CONTROLE", controle);
			newItemVO.setProperty("CODLOCALORIG", codLocalOrigem);
			newItemVO.setProperty("ATUALESTOQUE", atualizacaoEstoque);
			newItemVO.setProperty("RESERVA", reservaEstoque);

			newItemVO.setProperty("CODPROD", codProduto);
			newItemVO.setProperty("CODVOL", codVolume);
			newItemVO.setProperty("QTDNEG", quantidade);
			newItemVO.setProperty("VLRUNIT", valorUnitario);
			newItemVO.setProperty("VLRTOT", BigDecimalUtil.getRounded(quantidade.multiply(valorUnitario, mathCtx), 2));
			
			if (camposItemMap != null) {
				Set<String> nomesDeCampos = camposItemMap.keySet();
				
				for (String nomeCampo:nomesDeCampos) {
					newItemVO.setProperty(nomeCampo, camposItemMap.get(nomeCampo));
				}
			}

			return newItemVO;			
		}

		public BigDecimal getNuNota() {
			return nuNota;
		}
		
		public BigDecimal getSequencia() {
			return sequencia;
		}

		public DynamicVO getNewItemVO() {
			return this.newItemVO;
		}

		@Override
		public String toString() {
			return String.format("ItemNota [codProduto=%s, codVolume=%s, quantidade=%s, valorUnitario=%s, controle=%s, codLocalOrigem=%s, sequencia=%s, camposItemMap=%s, newItemVO=%s]", codProduto, codVolume, quantidade, valorUnitario, controle, codLocalOrigem, sequencia, camposItemMap, newItemVO);
		}
	}
	
	public class NotaConhecimento {
		private BigDecimal				numeroNota;
		private Timestamp				dataEmissao;
		private String					serie;
		private BigDecimal				valorNota;
		private BigDecimal				baseIcms;
		private BigDecimal				valorIcms;
		private BigDecimal				baseSt;
		private BigDecimal				valorSt;
		private BigDecimal				valorTotalProdutos;
		private BigDecimal				cfop;
		private String					chaveNfe;
		private BigDecimal				codModeloDocumento;
		private String					descricaoDocumento;
		
		public NotaConhecimento(BigDecimal numeroNota, Timestamp dataEmissao,
				String serie, BigDecimal valorNota, BigDecimal baseIcms,
				BigDecimal valorIcms, BigDecimal baseSt, BigDecimal valorSt,
				BigDecimal valorTotalProdutos, BigDecimal cfop,
				String chaveNfe, BigDecimal codModeloDocumento) throws Exception {

			buildNotaConhecimento(
					numeroNota, dataEmissao, serie, valorNota, baseIcms, 
					valorIcms, baseSt, valorSt, valorTotalProdutos, cfop, 
					chaveNfe, codModeloDocumento, null);
			
		}

		public NotaConhecimento(BigDecimal numeroNota, Timestamp dataEmissao,
				String serie, BigDecimal valorNota, BigDecimal baseIcms,
				BigDecimal valorIcms, BigDecimal baseSt, BigDecimal valorSt,
				BigDecimal valorTotalProdutos, BigDecimal cfop,
				String chaveNfe, BigDecimal codModeloDocumento,
				String descricaoDocumento) throws Exception {

			buildNotaConhecimento(
					numeroNota, dataEmissao, serie, valorNota, baseIcms, 
					valorIcms, baseSt, valorSt, valorTotalProdutos, cfop, 
					chaveNfe, codModeloDocumento, descricaoDocumento);

		}
		
		private void buildNotaConhecimento(BigDecimal numeroNota, Timestamp dataEmissao,
				String serie, BigDecimal valorNota, BigDecimal baseIcms,
				BigDecimal valorIcms, BigDecimal baseSt, BigDecimal valorSt,
				BigDecimal valorTotalProdutos, BigDecimal cfop,
				String chaveNfe, BigDecimal codModeloDocumento,
				String descricaoDocumento) throws Exception {
			
			this.numeroNota = numeroNota;
			this.dataEmissao = dataEmissao;
			this.serie = serie;
			this.valorNota = valorNota;
			this.baseIcms = baseIcms;
			this.valorIcms = valorIcms;
			this.baseSt = baseSt;
			this.valorSt = valorSt;
			this.valorTotalProdutos = valorTotalProdutos;
			this.cfop = cfop;
			this.chaveNfe = chaveNfe;
			this.codModeloDocumento = codModeloDocumento;
			this.descricaoDocumento = (descricaoDocumento == null) ? " " : descricaoDocumento;
			
			DynamicVO newCteNotaVO = buildNotaConhecimentoVO();
			
			PersistentLocalEntity cteNotaEntity = dwfEntityFacade.createEntity("NotaConhecimentoTransporte", (EntityVO) newCteNotaVO);
			newCteNotaVO = (DynamicVO) cteNotaEntity.getValueObject();
		}
		
		private DynamicVO buildNotaConhecimentoVO () throws Exception {
			DynamicVO cteNotaVO = (DynamicVO) dwfEntityFacade.getDefaultValueObjectInstance("NotaConhecimentoTransporte");

			cteNotaVO.setProperty("NUNOTA", nuNota);
			cteNotaVO.setProperty("SEQUENCIA", null);
			cteNotaVO.setProperty("NUMERO", numeroNota.toString());
			cteNotaVO.setProperty("DTEMISSAO", dataEmissao);
			cteNotaVO.setProperty("SERIE", serie);
			cteNotaVO.setProperty("VLRNOTA", valorNota);
			cteNotaVO.setProperty("CODMODDOC", codModeloDocumento);
			cteNotaVO.setProperty("BASEICMS", baseIcms);
			cteNotaVO.setProperty("VLRICMS", valorIcms);
			cteNotaVO.setProperty("BASEST", baseSt);
			cteNotaVO.setProperty("VLRST", valorSt);
			cteNotaVO.setProperty("VLRTOTPROD", valorTotalProdutos);
			cteNotaVO.setProperty("CFOP", cfop);
			cteNotaVO.setProperty("CHAVENFE", chaveNfe);
			cteNotaVO.setProperty("DESCRDOC", descricaoDocumento);
			
			return cteNotaVO;
		}

	}
	
	public class UnidadeMedida {
		private String					unidadeMedida;
		private String					tipoMedida;
		private	BigDecimal				quantidadeMedida;
		
		public UnidadeMedida(String unidadeMedida, String tipoMedida,
				BigDecimal quantidadeMedida) throws Exception {
			this.unidadeMedida = unidadeMedida;
			this.tipoMedida = tipoMedida;
			this.quantidadeMedida = quantidadeMedida;
			
			DynamicVO newUnidMedidaVO = buildUnidadeMedidaVO();
			
			PersistentLocalEntity unidMedidaEntity = dwfEntityFacade.createEntity("UnidadeMedida", (EntityVO) newUnidMedidaVO);
			newUnidMedidaVO = (DynamicVO) unidMedidaEntity.getValueObject();
		}
		
		private DynamicVO buildUnidadeMedidaVO () throws Exception {
			DynamicVO unidadeMedidaVO = (DynamicVO) dwfEntityFacade.getDefaultValueObjectInstance("UnidadeMedida");
			
			unidadeMedidaVO.setProperty("NUNOTA", nuNota);
			unidadeMedidaVO.setProperty("SEQMED", null);
			unidadeMedidaVO.setProperty("UNMED", unidadeMedida);
			unidadeMedidaVO.setProperty("TIPMED", tipoMedida);
			unidadeMedidaVO.setProperty("QTDMED", quantidadeMedida);
			
			return unidadeMedidaVO;
		}
	}
	
	public class SeguroTransporte {
		private String					responsavelSeguro;
		
		public SeguroTransporte() throws Exception {
			this.responsavelSeguro = "0";
			
			DynamicVO newSeguroTransporteVO = buildSeguroVO();
			
			PersistentLocalEntity seguroEntity = dwfEntityFacade.createEntity("SeguroTransporte", (EntityVO) newSeguroTransporteVO);
			newSeguroTransporteVO = (DynamicVO) seguroEntity.getValueObject();
		}
		
		private DynamicVO buildSeguroVO () throws Exception {
			DynamicVO seguroVO = (DynamicVO) dwfEntityFacade.getDefaultValueObjectInstance("SeguroTransporte");
			
			seguroVO.setProperty("NUNOTA", nuNota);
			seguroVO.setProperty("SEQSEG", null);
			seguroVO.setProperty("RESPSEG", responsavelSeguro);
			
			return seguroVO;
		}
	}
	
	public void confirmaNota() throws Exception {
        StringBuilder sbMensagemRetorno = new StringBuilder();
        
		bRegras = cacHelper.processarConfirmacao(nuNota);

		Collection<LiberacaoSolicitada> liberacoes = bRegras.getLiberacoesSolicitadas();

		if (!liberacoes.isEmpty()) {
			sbMensagemRetorno.append(String
					.format("N�o foi poss�vel confirmar o lan�amento NUNOTA %s."
							, nuNota ));
			sbMensagemRetorno.append("\nExiste(m) liberacao(oes) pendente(s):");

			for (LiberacaoSolicitada liberacao:liberacoes) {
				sbMensagemRetorno.append(String
						.format("\nEvento %s - %s."
								, liberacao.getEvento()
								, liberacao.getDescricao() ));
			}
			
			throw new MGEModelException(String
					.format("%s %s"
							, logAcao.getIdLog()
							, sbMensagemRetorno.toString() ));
		}
	        
	}
	
	public void confirmaNotaComAvisos(BigDecimal nuNota) throws Exception {
		final BarramentoRegra barramento = BarramentoRegra.build(CentralFaturamento.class, "regrasConfirmacaoSilenciosa.xml", AuthenticationInfo.getCurrent());
		barramento.setValidarSilencioso(true);
		
		PrePersistEntityState pre = ConfirmacaoNotaHelper.confirmarNota(nuNota, barramento, true);
	}

}

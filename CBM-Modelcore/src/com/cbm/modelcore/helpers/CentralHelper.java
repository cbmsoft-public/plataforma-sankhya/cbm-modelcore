package com.cbm.modelcore.helpers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jdom.Element;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.bmp.PersistentLocalEntity;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.vo.EntityVO;
import br.com.sankhya.jape.vo.PrePersistEntityState;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.modelcore.auth.AuthenticationInfo;
import br.com.sankhya.modelcore.comercial.BarramentoRegra;
import br.com.sankhya.modelcore.comercial.ConfirmacaoNotaHelper;
import br.com.sankhya.modelcore.comercial.centrais.CACHelper;
import br.com.sankhya.modelcore.comercial.CentralFaturamento;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.sankhya.ws.ServiceContext;

import com.cbm.modelcore.utils.LogUtils;

/**
 * Helper para inclusao de lancamentos nas centrais
 * 
 * @author Cassio MENEZES <cassio.menezes@outlook.com.br>
 * @version 0.1.1.03
 */
public class CentralHelper {
	//identificador para log
	private LogUtils							logAcao = new LogUtils(this.getClass().getSimpleName(), 0, 1, 0, 3);

	private EntityFacade						dwfEntityFacade = EntityFacadeFactory.getDWFFacade();
	private CACHelper							cacHelper = new CACHelper();
	private DynamicVO							newCabVO;
	private DynamicVO							oldCabVO;
	private BigDecimal							nuNota;
	private Collection<PrePersistEntityState>	itens = new ArrayList<PrePersistEntityState>();
	
	public CentralHelper() {
		// TODO Auto-generated constructor stub
	}

	public DynamicVO getNewCabVO() {
		return newCabVO;
	}

	public void setNewCabVO(DynamicVO newCabVO) {
		this.newCabVO = newCabVO;
	}

	public DynamicVO getOldCabVO() {
		return oldCabVO;
	}

	public void setOldCabVO(DynamicVO oldCabVO) {
		this.oldCabVO = oldCabVO;
	}
	
	public BigDecimal getNuNota() {
		return nuNota;
	}

	public void setNuNota(BigDecimal nuNota) {
		this.nuNota = nuNota;
	}

	public Collection<PrePersistEntityState> getItens() {
		return itens;
	}

	public void setItens(Collection<PrePersistEntityState> itens) {
		this.itens = itens;
	}

	public void incluirAlterarCabecalho(DynamicVO newCabVO, DynamicVO oldCabVO) throws Exception {
		this.newCabVO = newCabVO;
		this.oldCabVO = oldCabVO;
		
		incluirAlterarCabecalho();
	}
	
	public BarramentoRegra incluirAlterarCabecalho() throws Exception {
		ServiceContext serviceCtx = ServiceContext.getCurrent();
		
		BigDecimal newNuNota = newCabVO.asBigDecimal("NUNOTA");
		PersistentLocalEntity newCabEntity = null;
		if (newNuNota == null) {
			newCabEntity = dwfEntityFacade.createEntity(DynamicEntityNames.CABECALHO_NOTA, (EntityVO) newCabVO);
		} else {
			newCabEntity = dwfEntityFacade.findEntityByPrimaryKey(DynamicEntityNames.CABECALHO_NOTA, new Object[] { newNuNota });
		}
		
		PrePersistEntityState cabState = PrePersistEntityState.build(dwfEntityFacade, DynamicEntityNames.CABECALHO_NOTA, newCabVO, oldCabVO, newCabEntity);
		
		BarramentoRegra bRegras = cacHelper.incluirAlterarCabecalho((AuthenticationInfo) serviceCtx.getAutentication(), cabState);
		
		this.nuNota = newCabVO.asBigDecimal("NUNOTA");
		
		return bRegras;
	}

	public void incluirAlterarItem(Element itensElem, boolean inicializaProdutos) throws Exception {
		for (Element itemElem : (List<Element>) itensElem.getChildren("item")) {
			PrePersistEntityState itemNotaState = CACHelper.buildPrePersistState(itemElem, "ItemNota");

			itens.add(itemNotaState);
		}
		
		cacHelper.incluirAlterarItem(nuNota, ServiceContext.getCurrent(), itens, inicializaProdutos);
	}

	public void incluirAlterarItem(DynamicVO itemVO, DynamicVO itemOrigemVO, boolean inicializaProdutos) throws Exception {
		PersistentLocalEntity entity = null;
		if (itemVO.getPrimaryKey() == null) {
			entity = dwfEntityFacade.createEntity(DynamicEntityNames.ITEM_NOTA, (EntityVO) itemVO);
			
			PrePersistEntityState itemNotaState = PrePersistEntityState.build(dwfEntityFacade, DynamicEntityNames.ITEM_NOTA, itemVO, itemVO, entity);
			
//	        PrePersistEntityState itemNotaState = CACHelper.buildPrePersistState(null, "ItemNota", itemVO);
	        itemVO = itemNotaState.getNewVO();

	        itemVO.setAceptTransientProperties(true);
	        
	        itens.add(itemNotaState);
	        
	        cacHelper.incluirAlterarItem(nuNota, ServiceContext.getCurrent(), itens, inicializaProdutos);
	        
	        if (itemOrigemVO != null)
	        	ligaItemLancamentos(itemOrigemVO, itemVO);
		}
	}
	
	public void ligaItemLancamentos(DynamicVO itemOrigemVO, DynamicVO itemVO) throws Exception {
		JapeWrapper variosPedidoDAO = JapeFactory.dao(DynamicEntityNames.COMPRA_VENDA_VARIOS_PEDIDO);
		
		variosPedidoDAO
		.create()
		.set("NUNOTA", itemVO.asBigDecimal("NUNOTA"))
		.set("NUNOTAORIG", itemOrigemVO.asBigDecimal("NUNOTA"))
		.set("SEQUENCIA", itemVO.asBigDecimal("SEQUENCIA"))
		.set("SEQUENCIAORIG", itemOrigemVO.asBigDecimal("SEQUENCIA"))
		.set("QTDATENDIDA", itemOrigemVO.asBigDecimal("QTDPENDENTE"))
		.save();
	}

	public static PrePersistEntityState confirmaNotaComAvisos(BigDecimal nuNota) throws Exception {
		final BarramentoRegra barramento = BarramentoRegra.build(CentralFaturamento.class, "regrasConfirmacaoSilenciosa.xml", AuthenticationInfo.getCurrent());
		barramento.setValidarSilencioso(true);
		
		PrePersistEntityState pre = ConfirmacaoNotaHelper.confirmarNota(nuNota, barramento, true);
		
		return pre;
	}

	@Override
	public String toString() {
		return String.format("CentralHelper [logAcao=%s, newCabVO=%s]",
				logAcao, newCabVO);
	}

}

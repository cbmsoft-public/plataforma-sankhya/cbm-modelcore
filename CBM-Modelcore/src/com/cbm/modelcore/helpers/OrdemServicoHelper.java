package com.cbm.modelcore.helpers;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.bmp.PersistentLocalEntity;
import br.com.sankhya.jape.dao.EntityPrimaryKey;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.vo.EntityVO;
import br.com.sankhya.jape.wrapper.JapeFactory;
import br.com.sankhya.jape.wrapper.JapeWrapper;
import br.com.sankhya.jape.wrapper.fluid.FluidCreateVO;
import br.com.sankhya.mgeserv.model.utils.OrdemServicoUtil;
import br.com.sankhya.modelcore.auth.AuthenticationInfo;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;

import com.cbm.modelcore.utils.ItemOrdemServico;
import com.cbm.modelcore.utils.LogUtils;
import com.cbm.modelcore.utils.OrdemServico;
import com.sankhya.util.TimeUtils;

/**
 * Acao para geracao de lancamento de pedido de remessa
 * 
 * @author Cassio MENEZES <cassio.menezes@outlook.com.br>
 * @version 2.1.0.06
 */
public class OrdemServicoHelper {
	private LogUtils						logAcao;
	private EntityFacade 					dwfEntityFacade;

	private Timestamp						dataHoraAtual;
	private Timestamp						dataAtual;
	private BigDecimal						codigoUsuarioLogado;
	
	private BigDecimal						numeroOrdemServico;
	private DynamicVO						ordemServicoVO;
	private OrdemServico					cabecalhoOrdemServico;
	
	public OrdemServicoHelper() {
		this.logAcao = new LogUtils(this.getClass().getSimpleName(), 2, 1, 0, 5);
		this.dwfEntityFacade = EntityFacadeFactory.getDWFFacade();
		
		this.dataHoraAtual = new Timestamp(System.currentTimeMillis());
		this.dataAtual = new Timestamp(TimeUtils.clearTime(System.currentTimeMillis()));
		this.codigoUsuarioLogado = AuthenticationInfo.getCurrent().getUserID();
	}

	public BigDecimal getNumeroOrdemServico() {
		return numeroOrdemServico;
	}

	public void setNumeroOrdemServico(BigDecimal numeroOrdemServico) {
		this.numeroOrdemServico = numeroOrdemServico;
	}
	
	public OrdemServico getCabecalhoOrdemServico() {
		return cabecalhoOrdemServico;
	}

	public void setCabecalhoOrdemServico(OrdemServico cabecalhoOrdemServico) {
		this.cabecalhoOrdemServico = cabecalhoOrdemServico;
	}

	public void setCabecalhoOrdemServico(BigDecimal numeroContrato, BigDecimal codigoParceiro, BigDecimal codigoContato, String descricao, Map<String, Object> camposOrdemServico) {
		this.cabecalhoOrdemServico = new OrdemServico(numeroContrato, codigoParceiro, codigoContato, descricao, camposOrdemServico);
	}
	
	public void addItemOrdemServico(ItemOrdemServico itemOS) throws Exception {
		OrdemServicoUtil.validarProdutoContrato(cabecalhoOrdemServico.getNumeroContrato().intValue(), itemOS.getCodigoProduto().intValue());
		
		this.cabecalhoOrdemServico.addItem(itemOS);
	}
	
	public void inicialize(BigDecimal numeroOrdemServico) throws Exception {
		this.numeroOrdemServico = numeroOrdemServico;
		inicialize();
	}
	
	private void inicialize() throws Exception {
		EntityPrimaryKey ondemServicoPK = new EntityPrimaryKey(new Object[] { numeroOrdemServico });
		
		this.ordemServicoVO = (DynamicVO) dwfEntityFacade.findEntityByPrimaryKeyAsVO(DynamicEntityNames.ORDEM_SERVICO, ondemServicoPK);
	}

	/**
	 * Insere registro de Ordem de Servico.
	 * M�todo deve ser executado dentro de uma transa��o ativa.
	 * 
	 * @param cabecalhoOrdemServico
	 * @return
	 * @throws Exception 
	 */
	public void insertOrdemServico () throws Exception {
		/*
		this.ordemServicoVO = (DynamicVO) dwfEntityFacade.getDefaultValueObjectInstance(DynamicEntityNames.ORDEM_SERVICO);
		
		ordemServicoVO.setProperty("NUMCONTRATO", cabecalhoOrdemServico.getNumeroContrato());
		ordemServicoVO.setProperty("CODPARC", cabecalhoOrdemServico.getCodigoParceiro());
		ordemServicoVO.setProperty("CODCONTATO", cabecalhoOrdemServico.getCodigoContato());
		ordemServicoVO.setProperty("CODATEND", codigoUsuarioLogado);
		ordemServicoVO.setProperty("DHCHAMADA", dataHoraAtual);
		ordemServicoVO.setProperty("CODUSURESP", codigoUsuarioLogado);
		ordemServicoVO.setProperty("DESCRICAO", cabecalhoOrdemServico.getDescricao());
		
		ordemServicoVO.setProperty("DTPREVISTA", dataAtual);
		ordemServicoVO.setProperty("CODCOS", BigDecimal.ONE);
		//TODO parametrizar
		if (cabecalhoOrdemServico.getCodigoServicoFluxo() != null) {
			ordemServicoVO.setProperty("CODSERVFLUXO", cabecalhoOrdemServico.getCodigoServicoFluxo());
			ordemServicoVO.setProperty("VARIACAOFLUXO", cabecalhoOrdemServico.getVariacaoFluxo());			
		}
		
		if (cabecalhoOrdemServico.getCamposOrdemServico() != null) {
			Set<String> nomesDeCampos = cabecalhoOrdemServico.getCamposOrdemServico().keySet();
			
			for (String nomeCampo:nomesDeCampos) {
				ordemServicoVO.setProperty(nomeCampo, cabecalhoOrdemServico.getCamposOrdemServico().get(nomeCampo));
			}
		}
		
		PersistentLocalEntity ordemServicoEntity = dwfEntityFacade.createEntity(DynamicEntityNames.ORDEM_SERVICO, (EntityVO) ordemServicoVO);
		this.ordemServicoVO = (DynamicVO) ordemServicoEntity.getValueObject();
		*/
		
		//FIXME remove log
		logAcao.registraLog(String.format("cabecalhoOrdemServico %s", cabecalhoOrdemServico));
		
		JapeWrapper ordemServicoDAO = JapeFactory.dao(DynamicEntityNames.ORDEM_SERVICO);
		FluidCreateVO ordemServicoFCVO = ordemServicoDAO.create();
		
		ordemServicoFCVO
			.set("NUMCONTRATO", cabecalhoOrdemServico.getNumeroContrato())
			.set("CODPARC", cabecalhoOrdemServico.getCodigoParceiro())
			.set("CODCONTATO", cabecalhoOrdemServico.getCodigoContato())
			.set("CODATEND", codigoUsuarioLogado)
			.set("DHCHAMADA", dataHoraAtual)
			.set("CODUSURESP", codigoUsuarioLogado)
			.set("DESCRICAO", cabecalhoOrdemServico.getDescricao())
			.set("DTPREVISTA", dataAtual)
			.set("CODCOS", BigDecimal.ONE)		
			.set("CODSERVFLUXO", cabecalhoOrdemServico.getCodigoServicoFluxo())
			.set("VARIACAOFLUXO", cabecalhoOrdemServico.getVariacaoFluxo());

		if (cabecalhoOrdemServico.getCamposOrdemServico() != null) {
			Set<String> nomesDeCampos = cabecalhoOrdemServico.getCamposOrdemServico().keySet();
			
			for (String nomeCampo:nomesDeCampos) {
				ordemServicoFCVO.set(nomeCampo, cabecalhoOrdemServico.getCamposOrdemServico().get(nomeCampo));
			}
		}
		
		this.ordemServicoVO = ordemServicoFCVO.save();

		/*
		this.ordemServicoVO = ordemServicoDAO
			.create()
			.set("NUMCONTRATO", cabecalhoOrdemServico.getNumeroContrato())
			.set("CODPARC", cabecalhoOrdemServico.getCodigoParceiro())
			.set("CODCONTATO", cabecalhoOrdemServico.getCodigoContato())
			.set("CODATEND", codigoUsuarioLogado)
			.set("DHCHAMADA", dataHoraAtual)
			.set("CODUSURESP", codigoUsuarioLogado)
			.set("DESCRICAO", cabecalhoOrdemServico.getDescricao())
			
			.set("DTPREVISTA", dataAtual)
			.set("CODCOS", BigDecimal.ONE)
			
			.set("CODSERVFLUXO", cabecalhoOrdemServico.getCodigoServicoFluxo())
			.set("VARIACAOFLUXO", cabecalhoOrdemServico.getVariacaoFluxo())

			.save();
		*/
		
		this.numeroOrdemServico = ordemServicoVO.asBigDecimal("NUMOS");
		
		//INSERIR ITENS DA OS
		for (ItemOrdemServico item : cabecalhoOrdemServico.getItensOrdemServico()) {
			insertItemOrdemServico(numeroOrdemServico, item);
		}
		
		logAcao.registraLog(String.format("Ordem de Servico Nro. %s inclu�da com sucesso", numeroOrdemServico));
	}

	public void insertItemOrdemServico (BigDecimal numeroOrdemServico, ItemOrdemServico item) throws Exception {
		DynamicVO itemOrdemServicoVO = (DynamicVO) dwfEntityFacade.getDefaultValueObjectInstance(DynamicEntityNames.ITEM_ORDEM_SERVICO);

		itemOrdemServicoVO.setProperty("NUMOS", numeroOrdemServico);
		itemOrdemServicoVO.setProperty("CODSERV", item.getCodigoServico());
		itemOrdemServicoVO.setProperty("CODPROD", item.getCodigoProduto());

		itemOrdemServicoVO.setProperty("GRAUDIFIC", item.getGrauDeDificuldade());
		itemOrdemServicoVO.setProperty("RETRABALHO", item.getRetrabalho());
		itemOrdemServicoVO.setProperty("DHPREVISTA", item.getDataHoraPrevista());
		itemOrdemServicoVO.setProperty("CODUSU", item.getCodigoUsuario());
		itemOrdemServicoVO.setProperty("COBRAR", item.getCobrar());
		itemOrdemServicoVO.setProperty("CODOCOROS", item.getMotivo());
		itemOrdemServicoVO.setProperty("LIBERADO", item.getLiberado());
		itemOrdemServicoVO.setProperty("CODUSUREM", item.getCodigoUsuarioRemessa());
		itemOrdemServicoVO.setProperty("CODSIT", item.getCodigoSituacao());
		itemOrdemServicoVO.setProperty("PRIORIDADE", item.getPrioridade());

		if (item.getCamposItemOrdemServico() != null) {
			Set<String> nomesDeCampos = item.getCamposItemOrdemServico().keySet();
			
			for (String nomeCampo:nomesDeCampos) {
				itemOrdemServicoVO.setProperty(nomeCampo, item.getCamposItemOrdemServico().get(nomeCampo));
			}
		}

		PersistentLocalEntity ordemServicoEntity = dwfEntityFacade.createEntity(DynamicEntityNames.ITEM_ORDEM_SERVICO, (EntityVO) itemOrdemServicoVO);
		itemOrdemServicoVO = (DynamicVO) ordemServicoEntity.getValueObject();
	}

}

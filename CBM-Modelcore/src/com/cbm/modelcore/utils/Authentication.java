package com.cbm.modelcore.utils;

import br.com.sankhya.jape.util.JapeSessionContext;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.modelcore.auth.AuthenticationInfo;
import br.com.sankhya.modelcore.dwfdata.vo.tsi.UsuarioVO;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.sankhya.ws.ServiceContext;
import com.sankhya.util.BigDecimalUtil;
import com.sankhya.util.TimeUtils;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class Authentication {
	private LogUtils logAcao;
	private ServiceContext ctx;
	private AuthenticationInfo authInfo;
	private AuthenticationInfo oldAuthInfo;
	private UsuarioVO newUsuVO;
	private UsuarioVO oldUsuVO;

	public Authentication() {
		this.logAcao = new LogUtils(getClass().getSimpleName(), Integer.valueOf(4), Integer.valueOf(3), Integer.valueOf(0), Integer.valueOf(1));
	}

	public void setServiceContext() {
		setServiceContext(BigDecimal.ZERO);
	}

	public void setServiceContext(BigDecimal codUsuario) {
		ServiceContext svcContext = null;

		try {
			svcContext = ServiceContext.getCurrent();
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (svcContext == null) {
			svcContext = new ServiceContext(null);
			setAuthentication(codUsuario);
		}

		this.ctx = svcContext;
		this.ctx.makeCurrent();
	}

	public void setAuthentication(BigDecimal codUsuario) {
		try {
			this.oldAuthInfo = ((AuthenticationInfo) ServiceContext.getCurrent().getAutentication());
		} catch (NullPointerException npe) {

		}

		try {
			this.newUsuVO = ((UsuarioVO) ((DynamicVO) EntityFacadeFactory.getDWFFacade().findEntityByPrimaryKeyAsVO("Usuario", new Object[] { codUsuario })).wrapInterface(UsuarioVO.class));
		} catch (Exception e) {

		}

		if (this.newUsuVO == null) {
			try {
				this.newUsuVO = ((UsuarioVO) ((DynamicVO) EntityFacadeFactory.getDWFFacade().findEntityByPrimaryKeyAsVO("Usuario", new Object[] { BigDecimal.ZERO })).wrapInterface(UsuarioVO.class));
			} catch (Exception e) {
				String errorMessage = e.getMessage() == null ? "SEM MENSAGEM DE ERRO" : e.getMessage();

				this.logAcao.registraLog("Usu�rio n�o encontrado: " + codUsuario.toString());
				this.logAcao.registraLog(errorMessage);

				e.printStackTrace();

				return;
			}
		}

		if (this.oldAuthInfo == null) {
			try {
				this.oldUsuVO = ((UsuarioVO) ((DynamicVO) EntityFacadeFactory.getDWFFacade().findEntityByPrimaryKeyAsVO("Usuario", new Object[] { BigDecimal.ZERO })).wrapInterface(UsuarioVO.class));
			} catch (Exception e) {

			}
		} else {
			this.oldUsuVO = this.oldAuthInfo.getUsuVO();
		}

		this.authInfo = new AuthenticationInfo(this.newUsuVO.getNOMEUSU(), this.newUsuVO.getCODUSU(), this.newUsuVO.getCODGRUPO(), new Integer(Integer.MAX_VALUE));
		this.authInfo.makeCurrent();

		try {
			this.ctx.setAutentication(this.authInfo);
		} catch (Exception e) {

		}

		JapeSessionContext.putProperty("usuario_logado", this.authInfo.getUserID());
		JapeSessionContext.putProperty("emp_usu_logado", BigDecimalUtil.getValueOrZero(this.newUsuVO.getCODEMP()));
		JapeSessionContext.putProperty("dh_atual", new Timestamp(System.currentTimeMillis()));
		JapeSessionContext.putProperty("d_atual", new Timestamp(TimeUtils.getToday()));
		JapeSessionContext.putProperty("usuarioVO", this.newUsuVO);
		JapeSessionContext.putProperty("authInfo", this.authInfo);
	}

	public void releaseAuthentication() {
		this.ctx.unregistry();
		AuthenticationInfo.unregistry();

		if (this.oldUsuVO != null) {
			this.authInfo = new AuthenticationInfo(this.oldUsuVO.getNOMEUSU(), this.oldUsuVO.getCODUSU(), this.oldUsuVO.getCODGRUPO(), new Integer(Integer.MAX_VALUE));
			this.authInfo.makeCurrent();

			JapeSessionContext.putProperty("usuario_logado", this.authInfo.getUserID());
			JapeSessionContext.putProperty("emp_usu_logado", BigDecimalUtil.getValueOrZero(this.oldUsuVO.getCODEMP()));
			JapeSessionContext.putProperty("dh_atual", new Timestamp(System.currentTimeMillis()));
			JapeSessionContext.putProperty("d_atual", new Timestamp(TimeUtils.getToday()));
			JapeSessionContext.putProperty("usuarioVO", this.oldUsuVO);
			JapeSessionContext.putProperty("authInfo", this.authInfo);
		}

		this.oldUsuVO = null;
		this.newUsuVO = null;
		this.oldAuthInfo = null;
		this.authInfo = null;
		this.ctx = null;
	}
}

package com.cbm.modelcore.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.cbm.modelcore.utils.ItemOrdemServico;

public class OrdemServico {
	private BigDecimal				numeroOrdemServico;
	private BigDecimal				numeroContrato;
	private BigDecimal				codigoParceiro;
	private BigDecimal				codigoContato;
	private String					descricao;

	private BigDecimal				codigoServicoFluxo;
	private BigDecimal				variacaoFluxo;
	private BigDecimal				numeroOrdemServicoRelacionada;
	private Map<String, Object>		camposOrdemServico;	
	
	private List<ItemOrdemServico>	itensOrdemServico = new ArrayList<ItemOrdemServico>();

	public OrdemServico() {
		
	}

	public OrdemServico(BigDecimal numeroContrato, BigDecimal codigoParceiro, BigDecimal codigoContato, String descricao, Map<String, Object> camposOrdemServico) {
		this.setNumeroContrato(numeroContrato);
		this.setCodigoParceiro(codigoParceiro);
		this.setCodigoContato(codigoContato);
		this.setDescricao(descricao);
		this.setCamposOrdemServico(camposOrdemServico);
	}

	public BigDecimal getNumeroOrdemServico() {
		return numeroOrdemServico;
	}

	public void setNumeroOrdemServico(BigDecimal numeroOrdemServico) {
		this.numeroOrdemServico = numeroOrdemServico;
	}

	public void setItensOrdemServico(List<ItemOrdemServico> itensOrdemServico) {
		this.itensOrdemServico = itensOrdemServico;
	}

	public BigDecimal getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(BigDecimal numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public BigDecimal getCodigoParceiro() {
		return codigoParceiro;
	}

	public void setCodigoParceiro(BigDecimal codigoParceiro) {
		this.codigoParceiro = codigoParceiro;
	}

	public BigDecimal getCodigoContato() {
		return codigoContato;
	}

	public void setCodigoContato(BigDecimal codigoContato) {
		this.codigoContato = codigoContato;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getCodigoServicoFluxo() {
		return codigoServicoFluxo;
	}

	public void setCodigoServicoFluxo(BigDecimal codigoServicoFluxo) {
		this.codigoServicoFluxo = codigoServicoFluxo;
	}

	public BigDecimal getVariacaoFluxo() {
		return variacaoFluxo;
	}

	public void setVariacaoFluxo(BigDecimal variacaoFluxo) {
		this.variacaoFluxo = variacaoFluxo;
	}

	public BigDecimal getNumeroOrdemServicoRelacionada() {
		return numeroOrdemServicoRelacionada;
	}

	public void setNumeroOrdemServicoRelacionada(BigDecimal numeroOrdemServicoRelacionada) {
		this.numeroOrdemServicoRelacionada = numeroOrdemServicoRelacionada;
	}

	public Map<String, Object> getCamposOrdemServico() {
		return camposOrdemServico;
	}

	public void setCamposOrdemServico(Map<String, Object> camposOrdemServico) {
		this.camposOrdemServico = camposOrdemServico;
	}
	
	public void addItem (ItemOrdemServico newItem) {
		itensOrdemServico.add(newItem);
	}

	public List<ItemOrdemServico> getItensOrdemServico() {
		return itensOrdemServico;
	}

	@Override
	public String toString() {
		return String.format("OrdemServico [numeroOrdemServico=%s, numeroContrato=%s, codigoParceiro=%s, codigoContato=%s, descricao=%s, codigoServicoFluxo=%s, variacaoFluxo=%s, numeroOrdemServicoRelacionada=%s, camposOrdemServico=%s, itensOrdemServico=%s]", numeroOrdemServico, numeroContrato, codigoParceiro, codigoContato, descricao, codigoServicoFluxo, variacaoFluxo, numeroOrdemServicoRelacionada, camposOrdemServico, itensOrdemServico);
	}

}

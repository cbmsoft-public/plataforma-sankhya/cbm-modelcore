package com.cbm.modelcore.utils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

import br.com.sankhya.modelcore.auth.AuthenticationInfo;

public class ItemOrdemServico {
	private BigDecimal				numeroOrdemServico;
	private BigDecimal				numeroItem;
	private BigDecimal				codigoUsuarioLogado;
	private BigDecimal				codigoServico;
	private BigDecimal				codigoProduto;
	private BigDecimal				grauDeDificuldade;
	private String					retrabalho;
	private long					dhPrevista;
	private Timestamp				dataHoraPrevista;
	private BigDecimal				codigoUsuario;
	private String					cobrar;
	private BigDecimal				motivo;
	private String					liberado;
	private BigDecimal				codigoUsuarioRemessa;
	private BigDecimal				codigoSituacao;
	private String					solucao;
	private Timestamp				inicioExecucao;
	private Timestamp				terminoExecucao;
	private BigDecimal				horaInicial;
	private BigDecimal				horaFinal;
	private long					dataExecucao;
	private BigDecimal				prioridade;
	
	private Map<String, Object>		camposItemOrdemServico;

	public ItemOrdemServico() {
		
	}

	public ItemOrdemServico(BigDecimal codigoServico, BigDecimal codigoProduto, BigDecimal codigoUsuario, BigDecimal motivo, BigDecimal codigoUsuarioRemessa, Timestamp dataHoraAtual, Map<String, Object> camposItemOrdemServico) {
		this.codigoUsuarioLogado = AuthenticationInfo.getCurrent().getUserID();

		this.codigoServico = codigoServico;
		this.codigoProduto = codigoProduto;
		this.codigoUsuario = (codigoUsuario == null) ? codigoUsuarioLogado : codigoUsuario;
		this.motivo = motivo;
		this.codigoUsuarioRemessa = (codigoUsuarioRemessa == null) ? codigoUsuarioLogado : codigoUsuarioRemessa;
		this.camposItemOrdemServico = camposItemOrdemServico;
		this.dataHoraPrevista = dataHoraAtual;
		
		this.grauDeDificuldade = BigDecimal.ZERO;
		this.retrabalho = "N";
		this.cobrar = "N";
		this.liberado = "N";
		this.codigoSituacao = BigDecimal.ONE;
	}
	
	public ItemOrdemServico buildClone() {
		ItemOrdemServico r = new ItemOrdemServico();
		r.numeroOrdemServico = numeroOrdemServico;
		r.numeroItem = numeroItem;
		r.codigoUsuarioLogado = codigoUsuarioLogado;
		r.codigoServico = codigoServico;
		r.codigoProduto = codigoProduto;
		r.grauDeDificuldade = grauDeDificuldade;
		r.retrabalho = retrabalho;
		r.dhPrevista = dhPrevista;
		r.dataHoraPrevista = dataHoraPrevista;
		r.codigoUsuario = codigoUsuario;
		r.cobrar = cobrar;
		r.motivo = motivo;
		r.liberado = liberado;
		r.codigoUsuarioRemessa = codigoUsuarioRemessa;
		r.codigoSituacao = codigoSituacao;
		r.solucao = solucao;
		r.inicioExecucao = inicioExecucao;
		r.terminoExecucao = terminoExecucao;
		r.horaInicial = horaInicial;
		r.horaFinal = horaFinal;
		r.dataExecucao = dataExecucao;
		r.prioridade = prioridade;

		return r;
	}

	public BigDecimal getNumeroOrdemServico() {
		return numeroOrdemServico;
	}

	public void setNumeroOrdemServico(BigDecimal numeroOrdemServico) {
		this.numeroOrdemServico = numeroOrdemServico;
	}

	public BigDecimal getNumeroItem() {
		return numeroItem;
	}

	public void setNumeroItem(BigDecimal numeroItem) {
		this.numeroItem = numeroItem;
	}

	public BigDecimal getCodigoUsuarioLogado() {
		return codigoUsuarioLogado;
	}

	public void setCodigoUsuarioLogado(BigDecimal codigoUsuarioLogado) {
		this.codigoUsuarioLogado = codigoUsuarioLogado;
	}

	public BigDecimal getCodigoServico() {
		return codigoServico;
	}

	public void setCodigoServico(BigDecimal codigoServico) {
		this.codigoServico = codigoServico;
	}

	public BigDecimal getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(BigDecimal codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public BigDecimal getGrauDeDificuldade() {
		return grauDeDificuldade;
	}

	public void setGrauDeDificuldade(BigDecimal grauDeDificuldade) {
		this.grauDeDificuldade = grauDeDificuldade;
	}

	public String getRetrabalho() {
		return retrabalho;
	}

	public void setRetrabalho(String retrabalho) {
		this.retrabalho = retrabalho;
	}

	public long getDhPrevista() {
		return dhPrevista;
	}

	public void setDhPrevista(long dhPrevista) {
		this.dhPrevista = dhPrevista;
	}

	public Timestamp getDataHoraPrevista() {
		return dataHoraPrevista;
	}

	public void setDataHoraPrevista(Timestamp dataHoraPrevista) {
		this.dataHoraPrevista = dataHoraPrevista;
	}

	public BigDecimal getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(BigDecimal codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public String getCobrar() {
		return cobrar;
	}

	public void setCobrar(String cobrar) {
		this.cobrar = cobrar;
	}

	public BigDecimal getMotivo() {
		return motivo;
	}

	public void setMotivo(BigDecimal motivo) {
		this.motivo = motivo;
	}

	public String getLiberado() {
		return liberado;
	}

	public void setLiberado(String liberado) {
		this.liberado = liberado;
	}

	public BigDecimal getCodigoUsuarioRemessa() {
		return codigoUsuarioRemessa;
	}

	public void setCodigoUsuarioRemessa(BigDecimal codigoUsuarioRemessa) {
		this.codigoUsuarioRemessa = codigoUsuarioRemessa;
	}

	public BigDecimal getCodigoSituacao() {
		return codigoSituacao;
	}

	public void setCodigoSituacao(BigDecimal codigoSituacao) {
		this.codigoSituacao = codigoSituacao;
	}

	public String getSolucao() {
		return solucao;
	}

	public void setSolucao(String solucao) {
		this.solucao = solucao;
	}

	public Timestamp getInicioExecucao() {
		return inicioExecucao;
	}

	public void setInicioExecucao(Timestamp inicioExecucao) {
		this.inicioExecucao = inicioExecucao;
	}

	public Timestamp getTerminoExecucao() {
		return terminoExecucao;
	}

	public void setTerminoExecucao(Timestamp terminoExecucao) {
		this.terminoExecucao = terminoExecucao;
	}

	public BigDecimal getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(BigDecimal horaInicial) {
		this.horaInicial = horaInicial;
	}

	public BigDecimal getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(BigDecimal horaFinal) {
		this.horaFinal = horaFinal;
	}

	public long getDataExecucao() {
		return dataExecucao;
	}

	public void setDataExecucao(long dataExecucao) {
		this.dataExecucao = dataExecucao;
	}

	public BigDecimal getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(BigDecimal prioridade) {
		this.prioridade = prioridade;
	}

	public Map<String, Object> getCamposItemOrdemServico() {
		return camposItemOrdemServico;
	}

	public void setCamposItemOrdemServico(Map<String, Object> camposItemOrdemServico) {
		this.camposItemOrdemServico = camposItemOrdemServico;
	}

	public void setDefaultValues() {
		this.codigoUsuarioRemessa = codigoUsuarioLogado;
		
		this.grauDeDificuldade = BigDecimal.ZERO;
		this.retrabalho = "N";
		this.cobrar = "N";
		this.liberado = "N";
		this.codigoSituacao = BigDecimal.ONE;
	}

	@Override
	public String toString() {
		return String.format("ItemOrdemServico [numeroOrdemServico=%s, numeroItem=%s, codigoUsuarioLogado=%s, codigoServico=%s, codigoProduto=%s, grauDeDificuldade=%s, retrabalho=%s, dhPrevista=%s, dataHoraPrevista=%s, codigoUsuario=%s, cobrar=%s, motivo=%s, liberado=%s, codigoUsuarioRemessa=%s, codigoSituacao=%s, solucao=%s, inicioExecucao=%s, terminoExecucao=%s, horaInicial=%s, horaFinal=%s, dataExecucao=%s, prioridade=%s, camposItemOrdemServico=%s]", numeroOrdemServico, numeroItem, codigoUsuarioLogado, codigoServico, codigoProduto, grauDeDificuldade, retrabalho, dhPrevista, dataHoraPrevista, codigoUsuario, cobrar, motivo, liberado, codigoUsuarioRemessa, codigoSituacao, solucao, inicioExecucao, terminoExecucao, horaInicial, horaFinal, dataExecucao, prioridade, camposItemOrdemServico);
	}

}

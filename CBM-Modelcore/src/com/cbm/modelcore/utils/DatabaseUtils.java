package com.cbm.modelcore.utils;

import java.util.Map;
import java.util.Set;

import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.jape.metadata.EntityMetaData;
import br.com.sankhya.jape.sql.NativeSql;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;

import com.sankhya.util.StringUtils;

/**
 * Conjunto de ferramentas e utilitários para utilizacao com banco de dados
 * Monta <code>queries</code> com <code>NativeSql</code> entre outros
 * 
 * @author Cassio MENEZES [cassio.menezes@outlook.com.br
 * @version 4.1.0.01
 */
public class DatabaseUtils {
	public static final long			TIME_TO_SLEEP	= Long.parseLong(System.getProperty("wms.integracao.sleep.ret.pedidos", "1000"));
	
	public static boolean isOracle() {
		boolean							isOracle = Boolean.FALSE;
		
		try {
			isOracle = EntityFacadeFactory.getDWFFacade().getJdbcWrapper().getDialect() == EntityMetaData.ORACLE_DIALECT;
		} catch (Exception e) {
			isOracle = Boolean.FALSE;
		}
		
		return isOracle;	
	}

	public static NativeSql buildQuery(JdbcWrapper jdbc, Class<?> classeOrigem, String nomeArquivoQuery) throws Exception {
		return buildQuery(jdbc, classeOrigem, nomeArquivoQuery, null);
	}

	public static NativeSql buildQuery(JdbcWrapper jdbc, Class<?> classeOrigem, String nomeArquivoQuery, Map<String, String> variaveisSubstituicaoMap) throws Exception {
		NativeSql						nativeSql = null;
		StringBuffer					sbQuery = new StringBuffer();

		nativeSql = new NativeSql(jdbc);
		nativeSql.loadSql(classeOrigem, nomeArquivoQuery); 
		
		sbQuery = nativeSql.getSqlBuf();
		
		if (isOracle()) {
			StringUtils.replaceString("nullValue", "NVL", sbQuery, Boolean.TRUE);
		} else {
			StringUtils.replaceString("nullValue", "ISNULL", sbQuery, Boolean.TRUE);
			StringUtils.replaceString("/*NOLOCK*/", "(nolock)", sbQuery, Boolean.TRUE);				
		}

		if (variaveisSubstituicaoMap != null) {
			Set<String> variaveisSubstituicao = variaveisSubstituicaoMap.keySet();
			
			for (String variavelSubstituicao:variaveisSubstituicao) {
				StringUtils.replaceString(variavelSubstituicao, variaveisSubstituicaoMap.get(variavelSubstituicao), sbQuery, Boolean.TRUE);									
			}
		}
		
		return nativeSql;
	}
	
	public static void setDeadlockPriotityLow (JdbcWrapper jdbc) throws Exception {
		NativeSql						lowDeadLock = null;
		
		try {
			if (!isOracle()) {
				lowDeadLock = new NativeSql(jdbc);
				lowDeadLock.executeUpdate("SET DEADLOCK_PRIORITY LOW");			
			}			
		} finally {
			NativeSql.releaseResources(lowDeadLock);
		}
	}
}

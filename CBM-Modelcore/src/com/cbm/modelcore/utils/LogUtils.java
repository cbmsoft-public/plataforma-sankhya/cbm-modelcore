package com.cbm.modelcore.utils;

/**
 * Conjunto de ferramentas e utilit�rios para mensagens e registros em log no ambiente Sankhya
 * Monta texto formatado com informa��es sobre a classe e vers�o para uso em mensagens
 * 
 * @author Cassio MENEZES [cassio.menezes@outlook.com.br
 * @version 4.1.0.01
 */
public class LogUtils {
	private String					completeVersion;
	private String					idLog;

	public LogUtils(String className, Object version, Object major, Object minor, Object build) {
				
		this.completeVersion = String.format("%1$d.%2$d.%3$d.%4$02d", version, major, minor, build);
		this.idLog = String
				.format("[%s %s] ", 
						(className == null) ? this.getClass().getSimpleName() : className, 
						completeVersion );
	}
	
	public LogUtils(String className, Object major, Object minor, Object patch) {
		this.completeVersion = String.format("%1$d.%2$db%3$03d", major, minor, patch);
		this.idLog = String
				.format("[%s %s] ", 
						(className == null) ? this.getClass().getSimpleName() : className, 
						completeVersion );		
	}

	public String getCompleteVersion() {
		return completeVersion;
	}

	public String getIdLog() {
		return idLog;
	}

	public void registraLog(String mensagem) {
		System.out.println(String
				.format("%s %s", 
						idLog, 
						(mensagem == null) ? "SEM MENSAGEM DE ERRO!" : mensagem ));
	}
	
	public static class TimeCount {
		private long					startTime;
		private long					endTime;
		private long					elapsedTime;
		private long					totalSecs;
		private long					hours;
		private long					minutes;
		private long					seconds;
		private String					timeCounter;
				
		public TimeCount() {
			this.startTime = 0;
			this.endTime = 0;
			this.elapsedTime = 0;
			this.totalSecs = 0;
			this.hours = 0;
			this.minutes = 0;
			this.seconds = 0;
			this.timeCounter = null;
		}

		public void startCounter () {
			this.startTime = System.nanoTime();
		}
		
		public void stopCounter () {
			this.endTime = System.nanoTime();
			this.elapsedTime = endTime - startTime;
			this.totalSecs = elapsedTime / 1000000000;
			
			this.hours = totalSecs / 3600;
			this.minutes = (totalSecs % 3600) / 60;
			this.seconds = totalSecs % 60;
			
			this.timeCounter = String.format("%02d:%02d:%02d", hours, minutes, seconds);
		}
		
		public void reset () {
			this.startTime = 0;
			this.endTime = 0;
			this.elapsedTime = 0;
			this.totalSecs = 0;
			this.hours = 0;
			this.minutes = 0;
			this.seconds = 0;
			this.timeCounter = null;
		}
		
		public String getTimeCounter () {
			return timeCounter;
		}
	}

	@Override
	public String toString() {
		return idLog;
	}
}

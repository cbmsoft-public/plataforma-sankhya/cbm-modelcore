package com.cbmsoft.modelcore;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VersionFactoryTest {
	
	@Before
	public void setUp() {
		
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test
	public void t01_VersionFactory_getCompleteVersion() {
		String version = null;
		
		VersionFactory versionFactory = new VersionFactory();
		version = "0.0b000";
		Assert.assertEquals("Vers�o n�o determinada", version, versionFactory.getCompleteVersion());

		versionFactory = new VersionFactory(1,1,1);
		version = "1.1b001";
		Assert.assertEquals("Vers�o n�o determinada", version, versionFactory.getCompleteVersion());
	}

	@Test
	public void t02_VersionFactory_toString() {
		String version = null;
		
		VersionFactory versionFactory = new VersionFactory();
		version = "[cbmSoft 0.0b000]";
		Assert.assertEquals("Vers�o n�o determinada", version, versionFactory.toString());

		versionFactory = new VersionFactory(1,1,1);
		version = "[cbmSoft 1.1b001]";
		Assert.assertEquals("Vers�o n�o determinada", version, versionFactory.toString());

		versionFactory = new VersionFactory("test", 1,1,1);
		version = "[test 1.1b001]";
		Assert.assertEquals("Vers�o n�o determinada", version, versionFactory.toString());
	}

}

package com.cbmsoft.modelcore.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.sankhya.modulemgr.MGESession;

public class VisualizadorArquivo extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletOutputStream outputStream = null;
		
		try {
			String chaveArquivo = request.getParameter("chaveArquivo");
			String descrArquivo = request.getParameter("descrArquivo");
			
			byte[] arquivo = (byte[])MGESession.getSessaoPai(request.getSession()).getAttribute(chaveArquivo);
			
			response.setContentType("application/pdf");
			response.setContentLength(arquivo.length);
			response.setHeader("Content-Disposition", "inline; filename="+descrArquivo+".pdf");
			response.addHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			outputStream = response.getOutputStream();
			outputStream.write(arquivo, 0, arquivo.length);
			
			if(outputStream != null) {
				outputStream.flush();
				outputStream.close();
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
